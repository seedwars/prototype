const { defaults } = require('jest-config');

const path = require('path');

const {
  coverageReporters,
  coveragePathIgnorePatterns,
  moduleFileExtensions,
  moduleDirectories,
} = defaults;

module.exports = {
  verbose: true,
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/*.{spec,test}.*',
  ],
  coverageDirectory: path.resolve(__dirname, '../../coverage'),
  coverageReporters,
  coveragePathIgnorePatterns,
  moduleFileExtensions,
  moduleDirectories,
  rootDir: path.resolve(__dirname, '../../src/app'),
  setupTestFrameworkScriptFile: path.resolve(__dirname, 'init.js'),
  testMatch: [ '**/*.spec.{js,jsx}' ],
};
