const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const dirtyChai = require('dirty-chai');

chai.use(dirtyChai);
chai.use(sinonChai);
chai.should();

global.chai = chai;
global.expect = chai.expect;
global.sinon = sinon;
global.sandbox = sinon.createSandbox();
global.localStorage = {
  store: {},
  clear: () => { this.store = {}; },
  getItem: key => this.store[key] || null,
  setItem: (key, value) => { this.store[key] = value.toString(); },
  removeItem: key => { delete this.store[key]; },
};

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

Enzyme.configure({ adapter: new Adapter() });
