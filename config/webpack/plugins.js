const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const { publicKey } = require('./../../secrets/google');

const google = { publicKey: `"${ publicKey }"` };
const AUTH = { google };
const authPlugin = new webpack.DefinePlugin({ AUTH });

const indexPlugin = new HtmlWebpackPlugin({
  template: 'src/assets/templates/index.html',
  id: 'seedwars',
  title: 'Seed Wars!',
  filename: 'index.html',
});

const reduxDevToolsPlugin = new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify('development') });

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: 'assets/styles/[name].css',
  chunkFilename: 'assets/styles/[name].css',
});

module.exports = {
  plugins: [
    authPlugin,
    indexPlugin,
    reduxDevToolsPlugin,
    miniCssExtractPlugin,
  ],
};
