const distFolder = 'dist';

module.exports = (path, root, modules, plugins) => Object.assign({}, modules, plugins, {
  mode: 'development',
  name: 'production',
  entry: {
    app: './src/app/index.js',
    styles: './src/assets/styles/main.scss',
    vendors: [
      './node_modules/font-awesome/css/font-awesome.min.css',
      './node_modules/material-design-lite/material.min.css',
      './node_modules/material-design-icons/iconfont/material-icons.css',
      './node_modules/bootstrap-css-only/css/bootstrap.min.css',
    ],
  },
  output: {
    path: path.resolve(root, `${ distFolder }`),
    filename: 'lib/js/[name].bundle.js',
  },
  devServer: {
    clientLogLevel: 'info',
    historyApiFallback: {
      rewrites: [
        { from: /\/lib\/(.*)/, to: context => `/lib/${ context.match[1] }` },
        { from: /.*/, to: '/' },
      ],
    },
    contentBase: `./${ distFolder }`,
  },
});
