const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const babelLoader = { loader: 'babel-loader' };

const cssLoader = {
  loader: 'css-loader',
  options: { minimize: true },
};

const postcssCssLoader = { ...cssLoader, options: { modules: true } };

const postcssRule = {
  test: /src\/app\/.*\.(css|scss)$/,
  use: [ MiniCssExtractPlugin.loader, postcssCssLoader, 'postcss-loader' ],
};

const babelRule = {
  test: /src\/app\/.*\.(js|jsx)$/,
  use: babelLoader,
};

const globalCssRule = {
  test: /src\/assets\/styles\/.*\.(css|scss)$/,
  use: [ MiniCssExtractPlugin.loader, cssLoader, 'sass-loader' ],
};

const vendorCssRule = {
  test: /node_modules\/.*\.css$/,
  use: [ MiniCssExtractPlugin.loader, cssLoader ],
};

const fontsFileLoader = {
  loader: 'file-loader',
  options: { name: 'assets/fonts/[name].[ext]' },
};

const fontsRule = {
  test: /\.(eot|svg|ttf|woff|woff2)$/,
  use: fontsFileLoader,
};

const imagesFileLoader = {
  loader: 'file-loader',
  options: { name: 'assets/images/[name].[ext]' },
};

const imagesRule = {
  test: /\.(jpg|png|gif)$/,
  use: imagesFileLoader,
};

const sourcemapsRule = {
  enforce: 'pre',
  test: /\.(js|jsx)$/,
  loader: 'source-map-loader',
};

module.exports = {
  module: {
    rules: [
      postcssRule,
      babelRule,
      globalCssRule,
      vendorCssRule,
      fontsRule,
      imagesRule,
      sourcemapsRule,
    ],
  },
  resolve: {
    alias: { src: path.resolve(__dirname, '../../src') },
    extensions: [ '.js', '.jsx', '.json', '.css' ],
  },
};
