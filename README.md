Seedwars - Prototype
===

| Branch | Pipe | Coverage |
| :----: | :--: | :------: |
| Development | [![pipeline status](https://gitlab.com/seedwars/prototype/badges/development/pipeline.svg)](https://gitlab.com/seedwars/prototype/commits/development) | [![coverage report](https://gitlab.com/seedwars/prototype/badges/development/coverage.svg)](https://gitlab.com/seedwars/prototype/commits/development) |
| Staging | [![pipeline status](https://gitlab.com/seedwars/prototype/badges/staging/pipeline.svg)](https://gitlab.com/seedwars/prototype/commits/staging) | [![coverage report](https://gitlab.com/seedwars/prototype/badges/staging/coverage.svg)](https://gitlab.com/seedwars/prototype/commits/staging) |
| Master | [![pipeline status](https://gitlab.com/seedwars/prototype/badges/master/pipeline.svg)](https://gitlab.com/seedwars/prototype/commits/master) | [![coverage report](https://gitlab.com/seedwars/prototype/badges/master/coverage.svg)](https://gitlab.com/seedwars/prototype/commits/master) |

[seedwars.darkhounds.net](http://seedwars.darkhounds.net/)

Description
---
Prototype for testing the concept of using PRNG seeds as a dueling mechanic.

Requirements
---
- nodeJs

Installation
---
- npm install

Development
---
- npm run lint:watch
- npm run tests:watch
- npm run coverage:watch
- npm run serve
- npm run serve:docker

Reporting:
---
- npm run lint
- npm run tests
- npm run coverage

Production
---
- npm run build
