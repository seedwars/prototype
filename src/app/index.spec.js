const applicationID = 'seedwars';
const expectedElement = 'some expected element';

describe('Given the index node module', () => {
  let bootstrap;

  beforeEach(() => {
    bootstrap = sandbox.spy();
    jest.doMock('./bootstrap', () => bootstrap);

    sandbox.stub(global.document, 'getElementById')
      .withArgs(applicationID)
      .returns(expectedElement);

    require('./index');
  });

  it('should bootstrap the expected element a single time', () => {
    bootstrap.withArgs(expectedElement)
      .should.have.been.calledOnce();
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
