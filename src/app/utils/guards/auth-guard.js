const authGuard = ({ state }) => !!(state && state.app && state.app.user && state.app.user.name);

module.exports = authGuard;
