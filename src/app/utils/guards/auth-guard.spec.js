describe('Given the auth Guard', () => {
  let guard;

  describe('when required', () => {
    beforeEach(() => {
      guard = require('./auth-guard');
    });

    it('should return a function', () => {
      guard.should.be.a('function');
    });

    describe('when the state is missing', () => {
      const props = {};

      verifyResult(props, false);
    });

    describe('when the app state is missing', () => {
      const props = { state: {} };

      verifyResult(props, false);
    });

    describe('when the app user state is missing', () => {
      const props = { state: { app: {} } };

      verifyResult(props, false);
    });

    describe('when the app user name state is missing', () => {
      const props = { state: { app: { user: {} } } };

      verifyResult(props, false);
    });

    describe('when the state as all the necessary values', () => {
      const props = { state: { app: { user: { name: 'some username' } } } };

      verifyResult(props, true);
    });

    function verifyResult(props, valid) {
      describe('and the guard is invoked', () => {
        let result;

        beforeEach(() => {
          result = guard(props);
        });

        it(`should return '${ valid }'`, () => {
          result.should.equal(valid);
        });
      });
    }
  });
});
