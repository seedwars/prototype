describe('Given the has character Guard', () => {
  let guard;

  describe('when required', () => {
    beforeEach(() => {
      guard = require('./has-character-guard');
    });

    it('should return a function', () => {
      guard.should.be.a('function');
    });

    describe('when the state is missing', () => {
      const props = {};

      verifyResult(props, false);
    });

    describe('when the app state is missing', () => {
      const props = { state: {} };

      verifyResult(props, false);
    });

    describe('when the app character state is missing', () => {
      const props = { state: { app: {} } };

      verifyResult(props, false);
    });

    describe('when the app character data state is missing', () => {
      const props = { state: { app: { character: {} } } };

      verifyResult(props, false);
    });

    describe('when the app character data name state is missing', () => {
      const props = { state: { app: { character: { data: {} } } } };

      verifyResult(props, false);
    });

    describe('when the state as all the necessary values', () => {
      const props = { state: { app: { character: { data: { name: 'some name' } } } } };

      verifyResult(props, true);
    });

    function verifyResult(props, valid) {
      describe('and the guard is invoked', () => {
        let result;

        beforeEach(() => {
          result = guard(props);
        });

        it(`should return '${ valid }'`, () => {
          result.should.equal(valid);
        });
      });
    }
  });
});
