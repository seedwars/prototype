const hasCharacterGuard = ({ state }) => !!(
  state &&
  state.app &&
  state.app.character &&
  state.app.character.data &&
  state.app.character.data.name
);

module.exports = hasCharacterGuard;
