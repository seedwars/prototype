describe('Given the has NO character Guard', () => {
  let guard;

  describe('when required', () => {
    beforeEach(() => {
      guard = require('./has-no-character-guard');
    });

    it('should return a function', () => {
      guard.should.be.a('function');
    });

    describe('and when the state is missing', () => {
      const props = {};

      verifyResult(props, false);
    });

    describe('and when the app state is missing', () => {
      const props = { state: {} };

      verifyResult(props, false);
    });

    describe('and when the app character state is missing', () => {
      const props = { state: { app: {} } };

      verifyResult(props, false);
    });

    describe('and when the app character data state is missing', () => {
      const props = { state: { app: { character: {} } } };

      verifyResult(props, false);
    });

    describe('and when the app character name data name state is set', () => {
      const props = { state: { app: { character: { data: { name: 'some name' } } } } };

      verifyResult(props, false);
    });

    describe('and when the app character name data name state is NOT set', () => {
      const props = { state: { app: { character: { data: { name: '' } } } } };

      verifyResult(props, true);
    });

    function verifyResult(props, valid) {
      describe('and the guard is invoked', () => {
        let result;

        beforeEach(() => {
          result = guard(props);
        });

        it(`should return '${ valid }'`, () => {
          result.should.equal(valid);
        });
      });
    }
  });
});
