const generateModuleConfig = (name, path, mock) => ({ name, path, mock });

const modules = [
  generateModuleConfig('authGuard', './auth-guard', 'auth guards'),
  generateModuleConfig('hasCharacterGuard', './has-character-guard', 'has character guards'),
  generateModuleConfig('hasNoCharacterGuard', './has-no-character-guard', 'auth no character guards'),
];

describe('Given the guards index module', () => {
  let module;

  beforeEach(() => {
    modules.forEach(({ path, mock }) => jest.doMock(path, () => `some expected '${ mock }'`));

    module = require('./index');
  });

  modules.forEach(({ name, mock }) => it(`should contain the '${ name }' module`, () => {
    expect(module[name]).to.equal(`some expected '${ mock }'`);
  }));

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
