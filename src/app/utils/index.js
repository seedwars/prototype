const guards = require('./guards');
const createStoreWithHistory = require('./create-store-with-history');

module.exports = { guards, createStoreWithHistory };
