const { routerMiddleware } = require('react-router-redux');

const {
  applyMiddleware,
  compose,
  createStore,
} = require('redux');

const { default: thunkMiddleware } = require('redux-thunk');

const { REDUX_DEVTOOLS_EXTENSION_COMPOSE } = require('./../constants');

const createStoreWithHistory = (reducers, history) => {
  const routerMiddlewareWithHistory = routerMiddleware(history);
  const appliedMiddlewares = applyMiddleware(routerMiddlewareWithHistory, thunkMiddleware);
  const composeEnhancers = REDUX_DEVTOOLS_EXTENSION_COMPOSE || compose;
  const composedMiddlewares = composeEnhancers(appliedMiddlewares);
  const store = createStore(reducers, composedMiddlewares);

  return store;
};

module.exports = createStoreWithHistory;
