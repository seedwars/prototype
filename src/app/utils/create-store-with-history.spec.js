const expectedReducers = 'some expected reducers';
const expectedHistory = 'some expected history';
const expectedStore = 'some expected store';
const expectedRouterMiddlewareWithHistory = 'some expected middlewares';
const expectedAppliedMiddlewares = 'some expected middlewares';
const expectedComposedMiddlewares = 'some expected composed middlewares';

describe('Given the createStoreWithHistory node module', () => {
  let mockedConstants, mockedRouterMiddleware, mockedThunkMiddleware,
    mockedApplyMiddleware, mockedRedux, mockedCreateStore, mockedCompose;

  beforeEach(() => {
    mockedConstants = { REDUX_DEVTOOLS_EXTENSION_COMPOSE: null };
    jest.doMock('./../constants', () => mockedConstants);

    mockedRouterMiddleware = sandbox.stub().returns(null);
    mockedRouterMiddleware
      .withArgs(expectedHistory)
      .returns(expectedRouterMiddlewareWithHistory);
    jest.doMock('react-router-redux', () => ({ routerMiddleware: mockedRouterMiddleware }));

    mockedThunkMiddleware = () => null;
    jest.doMock('redux-thunk', () => ({ default: mockedThunkMiddleware }));

    mockedApplyMiddleware = sandbox.stub().returns(null);
    mockedApplyMiddleware
      .withArgs(expectedRouterMiddlewareWithHistory, mockedThunkMiddleware)
      .returns(expectedAppliedMiddlewares);

    mockedCreateStore = sandbox.stub().returns(null);
    mockedCreateStore
      .withArgs(expectedReducers, expectedComposedMiddlewares)
      .returns(expectedStore);

    mockedCompose = sandbox.stub().returns(null);
    mockedCompose
      .withArgs(expectedRouterMiddlewareWithHistory)
      .returns(expectedComposedMiddlewares);

    mockedRedux = {
      applyMiddleware: mockedApplyMiddleware,
      createStore: mockedCreateStore,
      compose: mockedCompose,
    };
    jest.doMock('redux', () => mockedRedux);
  });

  describe('when the REDUX_DEVTOOLS_EXTENSION_COMPOSE is NOT set', () => {
    verifyStore();
  });

  describe('when the REDUX_DEVTOOLS_EXTENSION_COMPOSE is set', () => {
    beforeEach(() => {
      mockedConstants.REDUX_DEVTOOLS_EXTENSION_COMPOSE = sandbox.stub().returns(null);
      mockedConstants.REDUX_DEVTOOLS_EXTENSION_COMPOSE
        .withArgs(expectedRouterMiddlewareWithHistory)
        .returns(expectedComposedMiddlewares);
    });

    verifyStore();
  });

  function verifyStore() {
    describe('and the module is required', () => {
      let createStoreWithHistory;

      beforeEach(() => {
        createStoreWithHistory = require('./create-store-with-history');
      });

      describe('then executed', () => {
        let store;

        beforeEach(() => {
          store = createStoreWithHistory(expectedReducers, expectedHistory);
        });

        it('should return the expected store', () => {
          store.should.equal(expectedStore);
        });
      });
    });
  }

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
