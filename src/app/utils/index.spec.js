const generateModuleConfig = (name, path, mock) => ({ name, path, mock });

const modules = [
  generateModuleConfig('guards', './guards', 'guards'),
  generateModuleConfig('createStoreWithHistory', './create-store-with-history', 'create store with history'),
];

describe('Given the utils index module', () => {
  let module;

  beforeEach(() => {
    modules.forEach(({ path, mock }) => jest.doMock(path, () => `some expected '${ mock }'`));

    module = require('./index');
  });

  modules.forEach(({ name, mock }) => it(`should contain the '${ name }' module`, () => {
    expect(module[name]).to.equal(`some expected '${ mock }'`);
  }));

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
