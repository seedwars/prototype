const actions = require('./actions');
const reducers = require('./reducers');
const thunks = require('./thunks');

module.exports = { actions, reducers, thunks };
