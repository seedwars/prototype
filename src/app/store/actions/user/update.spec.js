const types = require('./types');

const expectedUser = 'some expected user';
const expectedAction = { type: types.USER_UPDATE, payload: expectedUser };

describe('Given the user Update action', () => {
  describe('when required', () => {
    let actionCreator;

    beforeEach(() => {
      actionCreator = require('./update');
    });

    it('should be function', () => {
      actionCreator.should.be.a('function');
    });

    describe('and is invoked', () => {
      let actualAction;

      beforeEach(() => {
        actualAction = actionCreator(expectedUser);
      });

      it('should return the expected action', () => {
        actualAction.should.eql(expectedAction);
      });
    });
  });
});
