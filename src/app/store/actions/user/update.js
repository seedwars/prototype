const { USER_UPDATE } = require('./types');

const updateAction = user => ({
  type: USER_UPDATE,
  payload: user,
});

module.exports = updateAction;
