const updateAction = require('./update');
const types = require('./types');

module.exports = {
  types,
  updateAction,
};
