const UserActions = require('./user');
const CharacterActions = require('./character');

module.exports = { UserActions, CharacterActions };
