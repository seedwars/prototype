const types = require('./types');

const expectedPayload = 'some expected payload';
const expectedAction = { type: types.CHARACTER_LOADING, payload: expectedPayload };

describe('Given the character Lading action', () => {
  describe('when required', () => {
    let actionCreator;

    beforeEach(() => {
      actionCreator = require('./loading');
    });

    it('should be function', () => {
      actionCreator.should.be.a('function');
    });

    describe('and is invoked', () => {
      let actualAction;

      beforeEach(() => {
        actualAction = actionCreator(expectedPayload);
      });

      it('should return the expected action', () => {
        actualAction.should.eql(expectedAction);
      });
    });
  });
});
