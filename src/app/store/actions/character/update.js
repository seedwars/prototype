const { CHARACTER_UPDATE } = require('./types');

const updateAction = user => ({
  type: CHARACTER_UPDATE,
  payload: user,
});

module.exports = updateAction;
