const loadingAction = require('./loading');
const savingAction = require('./saving');
const updateAction = require('./update');
const types = require('./types');

module.exports = {
  loadingAction,
  savingAction,
  updateAction,
  types,
};
