const { CHARACTER_LOADING } = require('./types');

const savingAction = user => ({
  type: CHARACTER_LOADING,
  payload: user,
});

module.exports = savingAction;
