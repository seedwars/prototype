const { CHARACTER_SAVING } = require('./types');

const savingAction = user => ({
  type: CHARACTER_SAVING,
  payload: user,
});

module.exports = savingAction;
