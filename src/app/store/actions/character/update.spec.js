const types = require('./types');

const expectedCharacter = 'some expected character';
const expectedAction = { type: types.CHARACTER_UPDATE, payload: expectedCharacter };

describe('Given the character Update action', () => {
  describe('when required', () => {
    let actionCreator;

    beforeEach(() => {
      actionCreator = require('./update');
    });

    it('should be function', () => {
      actionCreator.should.be.a('function');
    });

    describe('and is invoked', () => {
      let actualAction;

      beforeEach(() => {
        actualAction = actionCreator(expectedCharacter);
      });

      it('should return the expected action', () => {
        actualAction.should.eql(expectedAction);
      });
    });
  });
});
