const types = require('./types');

const expectedPayload = 'some expected payload';
const expectedAction = { type: types.CHARACTER_SAVING, payload: expectedPayload };

describe('Given the character Save action', () => {
  describe('when required', () => {
    let actionCreator;

    beforeEach(() => {
      actionCreator = require('./saving');
    });

    it('should be function', () => {
      actionCreator.should.be.a('function');
    });

    describe('and is invoked', () => {
      let actualAction;

      beforeEach(() => {
        actualAction = actionCreator(expectedPayload);
      });

      it('should return the expected action', () => {
        actualAction.should.eql(expectedAction);
      });
    });
  });
});
