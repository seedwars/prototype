const { characterService } = require('./../../../../services/game');
const {
  updateAction,
  loadingAction,
} = require('./../../../actions/character');

module.exports = id => dispatch => {
  dispatch(loadingAction(true));

  characterService.load(id).then(character => {
    dispatch(updateAction(character));
    dispatch(loadingAction(false));
  });
};
