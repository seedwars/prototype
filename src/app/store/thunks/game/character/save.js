const { characterService } = require('./../../../../services/game');
const {
  savingAction,
  updateAction,
} = require('./../../../actions/character');

module.exports = (id, character) => dispatch => {
  dispatch(savingAction(true));

  return characterService.save(id, character).then(() => {
    dispatch(updateAction(character));
    dispatch(savingAction(false));
  });
};
