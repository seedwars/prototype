const loadThunk = require('./load');
const saveThunk = require('./save');

module.exports = { loadThunk, saveThunk };
