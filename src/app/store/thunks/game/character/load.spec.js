const expectedCharacterId = 'some expected character id';
const expectedCharacter = 'some expected character';
const expectedLoadingAction = 'some expected loading action';
const expectedUpdateAction = 'some expected update action';

describe('Given the character Load thunk creator', () => {
  let thunkCreator;

  describe('when required as a node module', () => {
    let mockedCharacterService, mockedDispatch, mockedUpdateAction, mockedLoadingAction;

    beforeEach(() => {
      mockedDispatch = sandbox.spy();

      mockedCharacterService = { load: sandbox.stub() };
      jest.doMock('./../../../../services/game', () => ({ characterService: mockedCharacterService }));

      mockedUpdateAction = sandbox.stub();
      mockedLoadingAction = sandbox.stub();
      jest.doMock('./../../../actions/character', () => ({
        loadingAction: mockedLoadingAction,
        updateAction: mockedUpdateAction,
      }));

      thunkCreator = require('./load');
    });

    describe('and a thunk is created', () => {
      let thunk;

      beforeEach(() => {
        thunk = thunkCreator(expectedCharacterId);
      });

      describe('and the thunk is executed', () => {
        let promiseResolve;

        beforeEach(() => {
          const promise = new Promise(resolve => { promiseResolve = resolve; });

          mockedLoadingAction
            .withArgs(true)
            .returns(expectedLoadingAction);

          mockedCharacterService.load
            .withArgs(expectedCharacterId)
            .returns(promise);

          thunk(mockedDispatch);
        });

        it('should dispatch a loading action set to true', () => {
          mockedDispatch.withArgs(expectedLoadingAction).should.have.been.calledOnce();
        });

        describe('and when the promise is resolved', () => {
          beforeEach(() => {
            mockedDispatch.resetHistory();

            mockedUpdateAction
              .withArgs(expectedCharacter)
              .returns(expectedUpdateAction);

            mockedLoadingAction.resetBehavior();
            mockedLoadingAction
              .withArgs(false)
              .returns(expectedLoadingAction);

            promiseResolve(expectedCharacter);
          });

          it('should dispatch an update action with the expected character', () => {
            mockedDispatch.withArgs(expectedUpdateAction).should.have.been.calledOnce();
          });

          it('should dispatch a loading action set to false', () => {
            mockedDispatch.withArgs(expectedLoadingAction).should.have.been.calledOnce();
          });
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
