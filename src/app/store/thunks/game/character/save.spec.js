const expectedCharacterId = 'some expected character id';
const expectedCharacter = 'some expected character';
const expectedSavingAction = 'some expected saving action';
const expectedUpdateAction = 'some expected update action';

describe('Given the character Save thunk creator', () => {
  let thunkCreator;

  describe('when required as a node module', () => {
    let mockedCharacterService, mockedDispatch, mockedUpdateAction, mockedSavingAction;

    beforeEach(() => {
      mockedDispatch = sandbox.spy();

      mockedCharacterService = { save: sandbox.stub() };
      jest.doMock('./../../../../services/game', () => ({ characterService: mockedCharacterService }));

      mockedUpdateAction = sandbox.stub();
      mockedSavingAction = sandbox.stub();
      jest.doMock('./../../../actions/character', () => ({
        savingAction: mockedSavingAction,
        updateAction: mockedUpdateAction,
      }));

      thunkCreator = require('./save');
    });

    describe('and a thunk is created', () => {
      let thunk;

      beforeEach(() => {
        thunk = thunkCreator(expectedCharacterId, expectedCharacter);
      });

      describe('and the thunk is executed', () => {
        let promiseResolve;

        beforeEach(() => {
          const promise = new Promise(resolve => { promiseResolve = resolve; });

          mockedSavingAction
            .withArgs(true)
            .returns(expectedSavingAction);

          mockedCharacterService.save
            .withArgs(expectedCharacterId, expectedCharacter)
            .returns(promise);

          thunk(mockedDispatch);
        });

        it('should dispatch a saving action set to true', () => {
          mockedDispatch.withArgs(expectedSavingAction).should.have.been.calledOnce();
        });

        it('should save the expected character', () => {
          mockedCharacterService.save
            .withArgs(expectedCharacterId, expectedCharacter)
            .should.have.been.calledOnce();
        });

        describe('and when the promise is resolved', () => {
          beforeEach(() => {
            mockedDispatch.resetHistory();

            mockedUpdateAction
              .withArgs(expectedCharacter)
              .returns(expectedUpdateAction);

            mockedSavingAction.resetBehavior();
            mockedSavingAction
              .withArgs(false)
              .returns(expectedSavingAction);

            promiseResolve(expectedCharacter);
          });

          it('should dispatch an update action with the expected character', () => {
            mockedDispatch.withArgs(expectedUpdateAction).should.have.been.calledOnce();
          });

          it('should dispatch a saving action set to false', () => {
            mockedDispatch.withArgs(expectedSavingAction).should.have.been.calledOnce();
          });
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
