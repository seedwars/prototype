const OAuth2 = require('./../../../../services/google/oauth2');
const { updateAction } = require('./../../../actions/user');

const LoginThunk = () => dispatch => {
  const scope = 'email profile';
  const config = { scope };

  return OAuth2.login(config).then(() => {
    const user = OAuth2.getProfile();
    const action = updateAction(user);

    dispatch(action);
  });
};

module.exports = LoginThunk;
