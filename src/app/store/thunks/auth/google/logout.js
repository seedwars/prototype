const OAuth2 = require('./../../../../services/google/oauth2');
const { updateAction } = require('./../../../actions/user');

const logoutThunk = () => dispatch => {
  OAuth2.logout().then(() => {
    const action = updateAction();

    dispatch(action);
  });
};

module.exports = logoutThunk;
