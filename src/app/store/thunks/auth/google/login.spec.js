const expectedUser = 'some expected user';
const expectedAction = 'some expected action';

describe('Given the Google auth login thunk', () => {
  let mockedOAuth2, mockedOAuth2Login, mockedGetProfile, mockedLoginPromise,
    mockedUserActions, mockedUserActionsUpdate;

  beforeEach(() => {
    mockedLoginPromise = Promise.resolve();

    mockedOAuth2Login = sandbox.stub();
    mockedOAuth2Login
      .withArgs({ scope: 'email profile' })
      .returns(mockedLoginPromise);
    mockedGetProfile = sandbox.stub();
    mockedGetProfile.returns(expectedUser);
    mockedOAuth2 = { login: mockedOAuth2Login, getProfile: mockedGetProfile };

    mockedUserActionsUpdate = sandbox.stub();
    mockedUserActionsUpdate
      .withArgs(expectedUser)
      .returns(expectedAction);
    mockedUserActions = { updateAction: mockedUserActionsUpdate };

    jest.doMock('./../../../../services/google/oauth2', () => mockedOAuth2);
    jest.doMock('./../../../actions/user', () => mockedUserActions);
  });

  describe('when required', () => {
    let thunk;

    beforeEach(() => {
      thunk = require('./login');
    });

    it('should be a function', () => {
      thunk.should.be.a('function');
    });

    describe('and the method is invoked', () => {
      let injectedThunk;

      beforeEach(() => {
        injectedThunk = thunk();
      });

      it('should return a function', () => {
        injectedThunk.should.be.a('function');
      });

      describe('the injectedFunction is invoked', () => {
        const dispatcher = sandbox.spy();

        beforeEach(() => injectedThunk(dispatcher));

        it('should have dispatched a single time', () => {
          dispatcher.withArgs(expectedAction).should.have.been.calledOnce();
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
