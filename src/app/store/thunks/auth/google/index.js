const loginThunk = require('./login');
const logoutThunk = require('./logout');

module.exports = { loginThunk, logoutThunk };
