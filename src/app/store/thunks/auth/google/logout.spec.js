const expectedAction = 'some expected action';

describe('Given the Google auth logout thunk', () => {
  let mockedOAuth2, mockedOAuth2Logout, mockedLogoutPromise,
    mockedUserActions, mockedUserActionsUpdate;

  beforeEach(() => {
    mockedLogoutPromise = Promise.resolve();

    mockedOAuth2Logout = sandbox.stub();
    mockedOAuth2Logout
      .returns(mockedLogoutPromise);
    mockedOAuth2 = { logout: mockedOAuth2Logout };

    mockedUserActionsUpdate = sandbox.stub();
    mockedUserActionsUpdate
      .withArgs()
      .returns(expectedAction);
    mockedUserActions = { updateAction: mockedUserActionsUpdate };

    jest.doMock('./../../../../services/google/oauth2', () => mockedOAuth2);
    jest.doMock('./../../../actions/user', () => mockedUserActions);
  });

  describe('when required', () => {
    let thunk;

    beforeEach(() => {
      thunk = require('./logout');
    });

    it('should be a function', () => {
      thunk.should.be.a('function');
    });

    describe('and the method is invoked', () => {
      let injectedThunk;

      beforeEach(() => {
        injectedThunk = thunk();
      });

      it('should return a function', () => {
        injectedThunk.should.be.a('function');
      });

      describe('the injectedFunction is invoked', () => {
        const dispatcher = sandbox.spy();

        beforeEach(() => injectedThunk(dispatcher));

        it('should have dispatched a single time', () => {
          dispatcher.withArgs(expectedAction).should.have.been.calledOnce();
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
