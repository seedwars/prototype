const expectedAction = { type: 'foobar', payload: undefined };
const expectedUser = 'some expected user';
const expectedCharacter = 'some expected character';
const expectedCurrentState = { foo: 'bar' };

const generateMatrixConfig =
  (currentState, action, newState) =>
    ({ currentState, action, newState });

const matrix = [
  generateMatrixConfig(undefined, expectedAction, {
    user: expectedUser,
    character: expectedCharacter,
  }),
  generateMatrixConfig(expectedCurrentState, expectedAction, {
    ...expectedCurrentState,
    user: expectedUser,
    character: expectedCharacter,
  }),
];

describe('Given the store reducers Index module', () => {
  let module;

  describe('when required as a node module', () => {
    let mockedUserReducer, mockedCharacterReducer;

    beforeEach(() => {
      mockedUserReducer = sandbox.stub();
      mockedUserReducer.withArgs(undefined, expectedAction).returns(expectedUser);

      mockedCharacterReducer = sandbox.stub();
      mockedCharacterReducer.withArgs(undefined, expectedAction).returns(expectedCharacter);

      jest.doMock('./user', () => mockedUserReducer);
      jest.doMock('./character', () => mockedCharacterReducer);

      module = require('./index');
    });

    it('should be an Object', () => {
      module.should.be.an.instanceof(Object);
    });

    it('should contain a mainReducer method', () => {
      module.should.respondTo('mainReducer');
    });

    matrix.forEach(({ currentState, action, newState }) => {
      describe(`and the current state is set to '${ JSON.stringify(currentState) }'`, () => {
        describe(`and the action is of type '${ action.type }'`, () => {
          verifyNewState(currentState, action, newState);
        });
      });
    });

    function verifyNewState(currentState, action, newState) {
      describe('and the reducer is invoked', () => {
        let actualNewState;

        beforeEach(() => {
          actualNewState = module.mainReducer(currentState, action);
        });

        it(`should return the state of '${ newState }'`, () => {
          expect(actualNewState).to.eql(newState);
        });
      });
    }
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
