const {
  CHARACTER_LOADING,
  CHARACTER_SAVING,
  CHARACTER_UPDATE,
} = require('./../../actions/character/types');

const expectedPayload = 'some expected payload';
const expectedEmptyState = { data: {}, saving: false, loading: false, dirty: false };
const expectedUpdateState = { ...expectedEmptyState, data: expectedPayload };
const expectedSavingState = { ...expectedEmptyState, saving: expectedPayload };
const expectedLoadingState = { ...expectedEmptyState, loading: expectedPayload };

const unknownAction = { type: 'unknown type' };
const characterLoadingAction = { type: CHARACTER_LOADING, payload: expectedPayload };
const characterSavingAction = { type: CHARACTER_SAVING, payload: expectedPayload };
const characterUpdateAction = { type: CHARACTER_UPDATE, payload: expectedPayload };

const generateMatrixConfig =
  (currentState, action, newState) =>
    ({ currentState, action, newState });

const matrix = [
  generateMatrixConfig(undefined, unknownAction, expectedEmptyState),
  generateMatrixConfig(expectedEmptyState, unknownAction, expectedEmptyState),
  generateMatrixConfig(undefined, characterLoadingAction, expectedLoadingState),
  generateMatrixConfig(expectedEmptyState, characterLoadingAction, expectedLoadingState),
  generateMatrixConfig(undefined, characterSavingAction, expectedSavingState),
  generateMatrixConfig(expectedEmptyState, characterSavingAction, expectedSavingState),
  generateMatrixConfig(undefined, characterUpdateAction, expectedUpdateState),
  generateMatrixConfig(expectedEmptyState, characterUpdateAction, expectedUpdateState),
];

describe('Given the Character reducer', () => {
  let reducer;

  describe('when required as a node module', () => {
    beforeEach(() => {
      reducer = require('./index');
    });

    matrix.forEach(({ currentState, action, newState }) => {
      describe(`and the current state is set to '${ JSON.stringify(currentState) }'`, () => {
        describe(`and the action is of type '${ action.type }'`, () => {
          verifyNewState(currentState, action, newState);
        });
      });
    });

    function verifyNewState(currentState, action, newState) {
      describe('and the reducer is invoked', () => {
        let actualState;

        beforeEach(() => {
          actualState = reducer(currentState, action);
        });

        it(`should return the state of '${ JSON.stringify(newState) }'`, () => {
          expect(actualState).to.eql(newState);
        });
      });
    }
  });
});
