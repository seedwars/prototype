const {
  CHARACTER_LOADING,
  CHARACTER_SAVING,
  CHARACTER_UPDATE,
} = require('./../../actions/character/types');

const defaultState = { data: {}, saving: false, loading: false, dirty: false };

const characterReducer = (currentState = defaultState, { type, payload }) => {
  let newState;

  switch (type) {
    case CHARACTER_LOADING: newState = { ...currentState, loading: payload }; break;
    case CHARACTER_SAVING: newState = { ...currentState, saving: payload }; break;
    case CHARACTER_UPDATE: newState = { ...currentState, data: payload }; break;
    default: newState = currentState; break;
  }

  return newState;
};

module.exports = characterReducer;
