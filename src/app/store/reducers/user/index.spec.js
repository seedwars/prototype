const { USER_UPDATE } = require('./../../actions/user/types');

const expectedName = 'expected name';
const expectedEmail = 'expected email';
const expectedUrl = 'expected url';
const expectedNewState = {
  name: expectedName,
  email: expectedEmail,
  avatar: expectedUrl,
};

describe('Given the User reducer', () => {
  const mockedPayload = {
    getName: () => expectedName,
    getEmail: () => expectedEmail,
    getImageUrl: () => expectedUrl,
  };

  describe('when required', () => {
    let reducer;

    beforeEach(() => {
      reducer = require('./index');
    });

    it('should be a function', () => {
      reducer.should.be.an.instanceof(Function);
    });

    describe('and there is NO current state', () => {
      const currentState = undefined;

      verifyActionTypeUnknown(currentState, null);

      verifyActionTypeUserUpdate(currentState);
    });

    describe('and there is a current state', () => {
      const currentState = { foo: 'bar' };

      verifyActionTypeUnknown(currentState, currentState);

      verifyActionTypeUserUpdate(currentState);
    });

    function verifyActionTypeUnknown(currentState, expectedState) {
      describe('and the action type is unknown', () => {
        const action = { type: 'unknown action', payload: mockedPayload };

        describe('and the method is invoked', () => {
          let newState;

          beforeEach(() => {
            newState = reducer(currentState, action);
          });

          it('should return a null state', () => {
            expect(newState).to.eql(expectedState);
          });
        });
      });
    }

    function verifyActionTypeUserUpdate(currentState) {
      describe(`and the action is a '${ USER_UPDATE }'`, () => {
        const action = { type: USER_UPDATE };

        describe('and there is a payload', () => {
          beforeEach(() => {
            action.payload = mockedPayload;
          });

          describe('and the method is invoked', () => {
            let newState;

            beforeEach(() => {
              newState = reducer(currentState, action);
            });

            it('should return a new state', () => {
              newState.should.not.equal(currentState);
            });

            it('should return the expected state value', () => {
              newState.should.eql(expectedNewState);
            });
          });
        });

        describe('and there is NO payload', () => {
          beforeEach(() => {
            action.payload = undefined;
          });

          describe('and the method is invoked', () => {
            let newState;

            beforeEach(() => {
              newState = reducer(currentState, action);
            });

            it('should return an empty state', () => {
              expect(newState).to.be.null();
            });
          });
        });
      });
    }
  });
});
