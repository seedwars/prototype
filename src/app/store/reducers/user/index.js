const { USER_UPDATE } = require('./../../actions/user/types');

const userReducer = (currentState = null, { type, payload }) => {
  let newState = null;

  switch (type) {
    case USER_UPDATE: newState = payload ? parseUser(payload) : null; break;
    default: newState = currentState; break;
  }

  return newState;
};

const parseUser = payload => ({
  name: payload.getName(),
  email: payload.getEmail(),
  avatar: payload.getImageUrl(),
});

module.exports = userReducer;
