const userReducer = require('./user');
const characterReducer = require('./character');

const mainReducer = (currentState = {}, { type, payload }) => {
  const user = userReducer(currentState.user, { type, payload });
  const character = characterReducer(currentState.character, { type, payload });

  return {
    ...currentState,
    user,
    character,
  };
};

module.exports = { mainReducer };

