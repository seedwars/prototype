const React = require('react');
const { shallow } = require('enzyme');

const expectedLogin = 'some expected login';

describe('Given the login template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedButton = () => '';

    beforeEach(() => {
      jest.doMock('@material-ui/core', () => ({ Button: MockedButton }));

      template = require('./template-login');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template(expectedLogin);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single button component', () => {
        renderedContent.find(MockedButton).should.have.length.of(1);
      });

      describe('and the button component', () => {
        let button;

        beforeEach(() => {
          button = renderedContent.find(MockedButton);
        });

        it('should contain the expected onClick prop', () => {
          const { onClick } = button.props();

          onClick.should.equal(expectedLogin);
        });

        it('should contain the expected text', () => {
          button.childAt(0).text().should.equal('Login');
        });
      });
    });
  });
});
