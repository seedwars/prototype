const { MockedReactComponent, mockConnect } = require('./../../../../test/mocks');

const expectedLoginThunk = 'some expected login thunk';
const expectedLogoutThunk = 'some expected logout thunk';
const expectedPushAction = 'some expected push action';

const expectedTarget = 'some expected target';
const expectedUser = 'some expected user';
const expectedLogout = 'some expected logout';
const expectedLogin = 'some expected login';
const expectedPlay = 'some expected play';
const expectedElement = 'some expected element';
const expectedContent = 'some expected render';

describe('Given the MainMenu container', () => {
  describe('when required', () => {
    let ContainerClass, mockedTemplateLogin, mockedTemplateMenu, mockedTemplateComponent,
      mockedConnect, mockedLoginThunk, mockedLogoutThunk, mockedGoogleThunks, mockedPush,
      mockedReactRouterRedux;

    beforeEach(() => {
      jest.doMock('react', () => ({ Component: MockedReactComponent }));

      mockedTemplateLogin = sandbox.stub();
      jest.doMock('./template-login', () => mockedTemplateLogin);

      mockedTemplateMenu = sandbox.stub();
      jest.doMock('./template-menu', () => mockedTemplateMenu);

      mockedTemplateComponent = sandbox.stub();
      jest.doMock('./template', () => mockedTemplateComponent);

      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedLoginThunk = () => expectedLoginThunk;
      mockedLogoutThunk = () => expectedLogoutThunk;
      mockedGoogleThunks = { loginThunk: mockedLoginThunk, logoutThunk: mockedLogoutThunk };
      jest.doMock('./../../../store/thunks/auth/google', () => mockedGoogleThunks);

      mockedPush = sandbox.stub();
      mockedReactRouterRedux = { push: mockedPush };
      jest.doMock('react-router-redux', () => mockedReactRouterRedux);

      ContainerClass = require('./container');
    });

    describe('and when mapping the state to props', () => {
      describe('and the state has a user', () => {
        beforeEach(() => {
          mockedConnect.setState({ app: { user: expectedUser } });
        });

        verifyPropsUserIs(expectedUser);
      });

      describe('and the state has NO user', () => {
        beforeEach(() => {
          mockedConnect.setState({ app: { user: undefined } });
        });

        verifyPropsUserIs(null);
      });

      function verifyPropsUserIs(expectedValue) {
        describe('and the container is instantiated', () => {
          let container;

          beforeEach(() => {
            container = ContainerClass({});
          });

          it(`should have mapped the user property set to the expected value of '${ expectedValue }'`, () => {
            expect(container.props.user).to.equal(expectedValue);
          });
        });
      }
    });

    describe('and when mapping the dispatch to props', () => {
      let mockedDispatch;

      beforeEach(() => {
        mockedConnect.setState({ app: {} });

        mockedDispatch = sandbox.spy();
        mockedConnect.setDispatch(mockedDispatch);
      });

      describe('and the container is instantiated', () => {
        let container;

        beforeEach(() => {
          container = ContainerClass({});
        });

        describe('and logging in', () => {
          beforeEach(() => {
            container.props.login();
          });

          it('should dispatch the expected thunk a single time', () => {
            mockedDispatch.withArgs(expectedLoginThunk).should.have.been.calledOnce();
          });
        });

        describe('and logging out', () => {
          beforeEach(() => {
            container.props.logout();
          });

          it('should dispatch the expected thunk a single time', () => {
            mockedDispatch.withArgs(expectedLogoutThunk).should.have.been.calledOnce();
          });
        });

        describe('and the play method is executed', () => {
          beforeEach(() => {
            mockedPush.withArgs('/game/play').returns(expectedPushAction);

            container.props.play();
          });

          it('should dispatch the expected action a single time', () => {
            mockedDispatch.withArgs(expectedPushAction).should.have.been.calledOnce();
          });
        });
      });
    });

    describe('and when the container is instantiated', () => {
      let container;

      beforeEach(() => {
        mockedConnect.setState({ app: {} });

        container = ContainerClass({});
      });

      it('should be an instance of the React Component', () => {
        container.should.be.an.instanceOf(MockedReactComponent);
      });

      describe('and the menu is opened', () => {
        beforeEach(() => {
          container.state.isMenuOpened = true;
        });

        verifyWhenMenuIsSwitched(false);
        verifyWhenMenuIsClosed();
      });

      describe('and the menu is closed', () => {
        beforeEach(() => {
          container.state.isMenuOpened = false;
        });

        verifyWhenMenuIsSwitched(true);
        verifyWhenMenuIsClosed();
      });

      function verifyWhenMenuIsSwitched(expectedState) {
        describe('and when switching the menu state', () => {
          const expectedValue = 'some expected value';

          beforeEach(() => {
            container.state.unaffectedProperty = expectedValue;
            container.state.switchMenu(expectedTarget);
          });

          it('should have the same unaffectedProperty', () => {
            container.state.unaffectedProperty.should.equal(expectedValue);
          });

          it('should close the menu', () => {
            container.state.isMenuOpened.should.equal(expectedState);
          });

          it('should have the expected target', () => {
            container.state.menuTarget.should.equal(expectedTarget);
          });
        });
      }

      function verifyWhenMenuIsClosed() {
        describe('and when closing the menu ', () => {
          const expectedValue = 'some expected value';

          beforeEach(() => {
            container.state.unaffectedProperty = expectedValue;
            container.state.closeMenu();
          });

          it('should have the same unaffectedProperty', () => {
            container.state.unaffectedProperty.should.equal(expectedValue);
          });

          it('should close the menu', () => {
            container.state.isMenuOpened.should.be.false();
          });
        });
      }

      describe('and the user is set', () => {
        beforeEach(() => {
          const { switchMenu, isMenuOpened, closeMenu } = container.state;

          container.props.user = expectedUser;
          container.props.logout = expectedLogout;
          container.props.play = expectedPlay;
          container.state.menuTarget = expectedTarget;

          mockedTemplateMenu
            .withArgs(
              switchMenu,
              expectedTarget,
              isMenuOpened,
              closeMenu,
              expectedLogout,
              expectedPlay,
            )
            .returns(expectedElement);
        });

        verifyContent();
      });

      describe('and the user is NOT set', () => {
        beforeEach(() => {
          container.props.user = null;
          container.props.login = expectedLogin;

          mockedTemplateLogin
            .withArgs(expectedLogin)
            .returns(expectedElement);
        });

        verifyContent();
      });

      function verifyContent() {
        describe('and the instance is rendered', () => {
          let actualContent;

          beforeEach(() => {
            mockedTemplateComponent
              .withArgs(expectedElement)
              .returns(expectedContent);

            actualContent = container.render();
          });

          it('return the expected render', () => {
            actualContent.should.equal(expectedContent);
          });
        });
      }
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
