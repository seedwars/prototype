const React = require('react');
const { shallow } = require('enzyme');

const expectedElement = 'some expected element';
const expectedMenuClassName = 'some expected menu class name';

describe('Given the main-menu template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedAppBar = () => '';
    const MockedToolbar = () => '';
    const mockedStyles = { menu: expectedMenuClassName };

    beforeEach(() => {
      jest.doMock('@material-ui/core', () => ({ AppBar: MockedAppBar, Toolbar: MockedToolbar }));
      jest.doMock('./styles.scss', () => mockedStyles);

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template(expectedElement);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single AppBar component', () => {
        renderedContent.find(MockedAppBar).should.have.length.of(1);
      });

      describe('and the AppBar component', () => {
        let appBar;

        beforeEach(() => {
          appBar = renderedContent.find(MockedAppBar);
        });

        it('should contain the expected position prop', () => {
          const { position } = appBar.props();

          position.should.equal('static');
        });

        it('should contain a single Toolbar component', () => {
          appBar.find(MockedToolbar).should.have.length.of(1);
        });

        describe('and the Toolbar component', () => {
          let toolbar;

          beforeEach(() => {
            toolbar = appBar.find(MockedToolbar);
          });

          it('should have the expected className prop', () => {
            const { className } = toolbar.props();

            className.should.equal(expectedMenuClassName);
          });

          it('should contain the expected element', () => {
            toolbar.childAt(0).text().should.equal(expectedElement);
          });
        });
      });
    });
  });
});
