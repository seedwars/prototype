const React = require('react');
const { shallow } = require('enzyme');

const expectedTarget = 'some expected target';
const expectedAnchor = { vertical: 'top', horizontal: 'right' };
const expectedIsMenuOpened = 'some expected is menu opened';

describe('Given the login template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedIconButton = () => '';
    const MockedMenu = () => '';
    const MockedMenuItem = () => '';
    const MockedAccountCircle = () => '';

    beforeEach(() => {
      jest.doMock('@material-ui/core', () => ({
        IconButton: MockedIconButton,
        Menu: MockedMenu,
        MenuItem: MockedMenuItem,
      }));
      jest.doMock('@material-ui/icons', () => ({ AccountCircle: MockedAccountCircle }));

      template = require('./template-menu');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent, mockedSwitchMenu, mockedCloseMenu,
        mockedLogout, mockedPlay;

      beforeEach(() => {
        mockedSwitchMenu = sandbox.stub();
        mockedCloseMenu = sandbox.stub();
        mockedLogout = sandbox.stub();
        mockedPlay = sandbox.stub();

        HostComponent = () => template(
          mockedSwitchMenu,
          expectedTarget,
          expectedIsMenuOpened,
          mockedCloseMenu,
          mockedLogout,
          mockedPlay,
        );

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single IconButton component', () => {
        renderedContent.find(MockedIconButton).should.have.length.of(1);
      });

      describe('and the IconButton component', () => {
        let iconButton;

        beforeEach(() => {
          iconButton = renderedContent.find(MockedIconButton);
        });

        describe('and is clicked', () => {
          beforeEach(() => {
            iconButton.simulate('click', { target: expectedTarget });
          });

          it('should have switched the menu method', () => {
            mockedSwitchMenu.withArgs(expectedTarget).should.have.been.calledOnce();
          });
        });

        it('should contain a single AccountCircle component', () => {
          iconButton.find(MockedAccountCircle).should.have.length.of(1);
        });
      });

      it('should contain a single Menu component', () => {
        renderedContent.find(MockedMenu).should.have.length.of(1);
      });

      describe('and the menu component', () => {
        let menu;

        beforeEach(() => {
          menu = renderedContent.find(MockedMenu);
        });

        it('should contain the expected anchorEl property', () => {
          const { anchorEl } = menu.props();

          anchorEl.should.equal(expectedTarget);
        });

        it('should contain the expected anchorOrigin property', () => {
          const { anchorOrigin } = menu.props();

          anchorOrigin.should.eql(expectedAnchor);
        });

        it('should contain the expected transformOrigin property', () => {
          const { transformOrigin } = menu.props();

          transformOrigin.should.eql(expectedAnchor);
        });

        it('should contain the expected open property', () => {
          const { open } = menu.props();

          open.should.eql(expectedIsMenuOpened);
        });

        it('should contain the expected onClose property', () => {
          const { onClose } = menu.props();

          onClose.should.eql(mockedCloseMenu);
        });

        it('should contain 2 MenuItem components', () => {
          menu.find(MockedMenuItem).should.have.length.of(2);
        });

        describe('and the first MenuItem component', () => {
          let menuItem;

          beforeEach(() => {
            menuItem = menu.find(MockedMenuItem).at(0);
          });

          it('should contain the expected text', () => {
            menuItem.childAt(0).text().should.equal('Logout');
          });

          describe('and is clicked', () => {
            beforeEach(() => {
              menuItem.simulate('click');
            });

            it('should have closed the menu a single time', () => {
              mockedCloseMenu.should.have.been.calledOnce();
            });

            it('should have logged out a single time', () => {
              mockedLogout.should.have.been.calledOnce();
            });
          });
        });

        describe('and the second MenuItem component', () => {
          let menuItem;

          beforeEach(() => {
            menuItem = menu.find(MockedMenuItem).at(1);
          });

          it('should contain the expected text', () => {
            menuItem.childAt(0).text().should.equal('Play');
          });

          describe('and is clicked', () => {
            beforeEach(() => {
              menuItem.simulate('click');
            });

            it('should have closed the menu a single time', () => {
              mockedCloseMenu.should.have.been.calledOnce();
            });

            it('should have start the game a single time', () => {
              mockedPlay.should.have.been.calledOnce();
            });
          });
        });
      });
    });
  });
});
