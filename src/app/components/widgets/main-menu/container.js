const PropTypes = require('prop-types');
const { Component } = require('react');

const { connect } = require('react-redux');
const { push } = require('react-router-redux');

const { loginThunk, logoutThunk } = require('./../../../store/thunks/auth/google');

const templateLogin = require('./template-login');
const templateMenu = require('./template-menu');
const template = require('./template');

const mapStateToProps = state => ({ user: state.app.user || null });

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(loginThunk()),
  logout: () => dispatch(logoutThunk()),
  play: () => dispatch(push('/game/play')),
});

class MainMenuState extends Component {
  constructor(props) {
    super(props);

    const switchMenu = menuTarget => {
      this.setState(state => ({ ...state, menuTarget, isMenuOpened: !state.isMenuOpened }));
    };

    const closeMenu = () => {
      this.setState(state => ({ ...state, isMenuOpened: false }));
    };

    this.state = { isMenuOpened: false, switchMenu, closeMenu };
  }

  render() {
    const { user, logout, play, login } = this.props;
    const { switchMenu, menuTarget, isMenuOpened, closeMenu } = this.state;

    const element = user ?
      templateMenu(switchMenu, menuTarget, isMenuOpened, closeMenu, logout, play) :
      templateLogin(login);

    return template(element);
  }
}

MainMenuState.displayName = 'MainMenuState';
MainMenuState.propTypes = {
  user: PropTypes.shape({}),
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  play: PropTypes.func.isRequired,
};
MainMenuState.defaultProps = { user: null };

const MainMenuContainer = connect(mapStateToProps, mapDispatchToProps)(MainMenuState);

MainMenuContainer.displayName = 'MainMenuContainer';

module.exports = MainMenuContainer;
