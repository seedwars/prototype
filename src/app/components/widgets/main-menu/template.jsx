const React = require('react');

const {
  AppBar,
  Toolbar,
} = require('@material-ui/core');

const styles = require('./styles.scss');

module.exports = element => (
  <AppBar position="static">
    <Toolbar className={ styles.menu } >{ element }</Toolbar>
  </AppBar>
);
