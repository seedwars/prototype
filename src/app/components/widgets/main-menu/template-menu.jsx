const React = require('react');

const {
  IconButton,
  Menu,
  MenuItem,
} = require('@material-ui/core');

const { AccountCircle } = require('@material-ui/icons');

const templateMenu = (switchMenu, menuTarget, isMenuOpened, closeMenu, logout, play) => (
  <div>
    <IconButton onClick={ event => switchMenu(event.target) }><AccountCircle /></IconButton>
    <Menu
      anchorEl={ menuTarget }
      anchorOrigin={ { vertical: 'top', horizontal: 'right' } }
      transformOrigin={ { vertical: 'top', horizontal: 'right' } }
      open={ isMenuOpened }
      onClose={ closeMenu }
    >
      <MenuItem onClick={ () => { closeMenu(); logout(); } }>Logout</MenuItem>
      <MenuItem onClick={ () => { closeMenu(); play(); } }>Play</MenuItem>
    </Menu>
  </div>
);

module.exports = templateMenu;
