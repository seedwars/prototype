const React = require('react');

const { Button } = require('@material-ui/core');

module.exports = login => <Button onClick={ login }>Login</Button>;
