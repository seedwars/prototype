const core = require('./core');
const widgets = require('./widgets');
const pages = require('./pages');
const Main = require('./main');

module.exports = { core, widgets, pages, Main };
