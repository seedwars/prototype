const React = require('react');

module.exports = (Component, componentProps) => <Component { ...componentProps } />;
