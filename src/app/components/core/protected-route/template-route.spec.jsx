const React = require('react');
const { shallow } = require('enzyme');

const expectedPath = 'some expected path';
const expectedComponent = 'some expected component';

const expectedPropertyValue = 'some expected property value';

describe('Given the protected route template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedRoute = () => '';

    beforeEach(() => {
      jest.doMock('react-router', () => ({ Route: MockedRoute }));

      template = require('./template-route');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent, mockedProps;

      beforeEach(() => {
        mockedProps = { foo: expectedPropertyValue };

        HostComponent = () => template(expectedPath, expectedComponent, mockedProps);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single route component', () => {
        renderedContent.find(MockedRoute).should.have.length.of(1);
      });

      describe('and the route component', () => {
        let componentInstance;

        beforeEach(() => {
          componentInstance = renderedContent.find(MockedRoute);
        });

        it('should have the expected path property', () => {
          const { path } = componentInstance.props();

          path.should.eql(expectedPath);
        });

        it('should have the expected component property', () => {
          const { component } = componentInstance.props();

          component.should.eql(expectedComponent);
        });

        it('should have the expected property value', () => {
          const { foo } = componentInstance.props();

          foo.should.eql(expectedPropertyValue);
        });
      });
    });
  });
});
