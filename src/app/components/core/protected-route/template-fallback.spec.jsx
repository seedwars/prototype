const React = require('react');
const { shallow } = require('enzyme');

const expectedFallback = 'some expected fallback';

describe('Given the protected fallback template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedRedirect = () => '';

    beforeEach(() => {
      jest.doMock('react-router', () => ({ Redirect: MockedRedirect }));

      template = require('./template-fallback');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template(expectedFallback);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single redirect component', () => {
        renderedContent.find(MockedRedirect).should.have.length.of(1);
      });

      describe('and the redirect component', () => {
        let component;

        beforeEach(() => {
          component = renderedContent.find(MockedRedirect);
        });

        it('should have the expected to prop', () => {
          const { to } = component.props();

          to.should.eql(expectedFallback);
        });
      });
    });
  });
});
