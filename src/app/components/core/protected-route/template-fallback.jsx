const React = require('react');
const { Redirect } = require('react-router');

module.exports = fallback => <Redirect to={ fallback } />;
