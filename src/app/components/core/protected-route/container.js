const PropTypes = require('prop-types');

const { connect } = require('react-redux');

const renderFallback = require('./template-fallback');
const renderRoute = require('./template-route');

const mapStateToProps = state => ({ state });

const ProtectedRouteComponent = props => {
  const { path, guard, component, fallback, ...componentProps } = props;

  return guard(props) ?
    renderRoute(path, component, componentProps) :
    renderRoute(path, getFallback(fallback));
};

const getFallback = fallback => (fallback ? () => renderFallback(fallback) : () => null);

ProtectedRouteComponent.displayName = 'ProtectedRoute';
ProtectedRouteComponent.propTypes = {
  path: PropTypes.string.isRequired,
  guard: PropTypes.func.isRequired,
  component: PropTypes.func.isRequired,
  fallback: PropTypes.string,
};
ProtectedRouteComponent.defaultProps = { fallback: '' };

module.exports = connect(mapStateToProps)(ProtectedRouteComponent);
