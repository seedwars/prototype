const React = require('react');
const { shallow } = require('enzyme');

const expectedProps = { foo: 'bar' };

describe('Given the protected component template', () => {
  let template;

  describe('when required as a node module', () => {
    beforeEach(() => {
      template = require('./template-component');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent, MockedComponent;

      beforeEach(() => {
        MockedComponent = () => '';
        HostComponent = () => template(MockedComponent, expectedProps);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single component', () => {
        renderedContent.find(MockedComponent).should.have.length.of(1);
      });

      describe('and the component', () => {
        let component;

        beforeEach(() => {
          component = renderedContent.find(MockedComponent);
        });

        it('should have the expected props', () => {
          const props = component.props();

          props.should.eql(expectedProps);
        });
      });
    });
  });
});
