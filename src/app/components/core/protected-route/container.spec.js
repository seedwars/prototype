const { mockConnect } = require('./../../../../test/mocks');

const expectedPath = 'some expected path';
const expectedComponent = 'some expected component';
const expectedFallback = 'some expected fallback';
const expectedComponentProps = { foo: 'bar', okus: 'pokus', state: {} };
const expectedElement = 'some expected element';
const expectedContent = 'expected content';

describe('Given the protected route component', () => {
  describe('when required', () => {
    let ComponentClass, mockedConnect,
      mockedRenderFallback, mockedRenderRoute;

    beforeEach(() => {
      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedRenderFallback = sandbox.stub();
      jest.doMock('./template-fallback', () => mockedRenderFallback);

      mockedRenderRoute = sandbox.stub();
      jest.doMock('./template-route', () => mockedRenderRoute);

      ComponentClass = require('./container');
    });

    it('should be a function', () => {
      ComponentClass.should.be.a('function');
    });

    describe('and the route is allowed', () => {
      const props = {
        guard: () => true,
        path: expectedPath,
        component: expectedComponent,
        ...expectedComponentProps,
      };

      beforeEach(() => {
        mockedRenderRoute
          .withArgs(expectedPath, expectedComponent, expectedComponentProps)
          .returns(expectedContent);
      });

      verifyExpectedContent(props);
    });

    describe('and the route is NOT allowed', () => {
      describe('and there is a fallback', () => {
        const props = {
          guard: () => false,
          path: expectedPath,
          fallback: expectedFallback,
        };

        beforeEach(() => {
          mockedRenderFallback
            .withArgs(expectedFallback)
            .returns(expectedElement);

          mockedRenderRoute
            .withArgs(expectedPath, sinon.match(arg => (arg() === expectedElement)))
            .returns(expectedContent);
        });

        verifyExpectedContent(props);
      });

      describe('and there is NO fallback', () => {
        const props = {
          guard: () => false,
          path: expectedPath,
        };

        beforeEach(() => {
          mockedRenderRoute
            .withArgs(expectedPath, sinon.match(arg => (arg() === null)))
            .returns(expectedContent);
        });

        verifyExpectedContent(props);
      });
    });

    function verifyExpectedContent(props) {
      let content;

      beforeEach(() => {
        mockedConnect.setState({});

        content = ComponentClass(props);
      });

      it('should return the expected content', () => {
        content.should.equal(expectedContent);
      });
    }
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
