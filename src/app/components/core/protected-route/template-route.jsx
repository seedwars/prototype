const React = require('react');
const { Route } = require('react-router');

module.exports = (path, component, props) =>
  <Route path={ path } component={ component } { ...props } />;
