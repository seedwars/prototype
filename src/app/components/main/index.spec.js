const expectedModule = 'some expected module';

describe('Given the main Module index', () => {
  let module;

  beforeEach(() => {
    jest.doMock('./container', () => expectedModule);

    module = require('./index');
  });

  it('should be the expected module', () => {
    module.should.equal(expectedModule);
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
