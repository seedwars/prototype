const expectedAuthGuard = 'some expected auth guard';
const expectedContent = 'some expected template';

describe('Given the Main container', () => {
  describe('when required', () => {
    let container, mockedTemplate;

    beforeEach(() => {
      mockedTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedTemplate);

      jest.doMock('./../../utils/guards/auth-guard', () => expectedAuthGuard);

      container = require('./container');
    });

    it('should return a function', () => {
      container.should.be.a('function');
    });

    describe('and when instantiated', () => {
      let actualContent;

      beforeEach(() => {
        mockedTemplate.withArgs(expectedAuthGuard).returns(expectedContent);

        actualContent = container();
      });

      it('should have rendered the expected content', () => {
        actualContent.should.equal(expectedContent);
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
