const template = require('./template');

const authGuard = require('./../../utils/guards/auth-guard');

const MainContainer = () => template(authGuard);

MainContainer.displayName = 'MainContainer';

module.exports = MainContainer;
