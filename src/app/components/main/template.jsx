const React = require('react');
const { Route } = require('react-router');

const { ProtectedRoute } = require('./../core');
const { MainMenu } = require('./../widgets');
const { Home, Game } = require('./../pages');

const template = authGuard => (
  <div>
    <MainMenu />
    <Route exact path="/" component={ Home } />
    <ProtectedRoute path="/game" guard={ authGuard } component={ Game } fallback="/" />
  </div>
);

module.exports = template;
