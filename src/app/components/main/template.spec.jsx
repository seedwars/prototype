const React = require('react');
const { shallow } = require('enzyme');

const expectedAuthGuard = 'some expected auth guard';

describe('Given the main template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedRoute = () => '';
    const MockedProtectedRoute = () => '';
    const MockedMainMenu = () => '';
    const MockedHome = () => '';
    const MockedGame = () => '';

    beforeEach(() => {
      jest.doMock('react-router', () => ({ Route: MockedRoute }));
      jest.doMock('./../core', () => ({ ProtectedRoute: MockedProtectedRoute }));
      jest.doMock('./../widgets', () => ({ MainMenu: MockedMainMenu }));
      jest.doMock('./../pages', () => ({ Home: MockedHome, Game: MockedGame }));

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template(expectedAuthGuard);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single main menu component', () => {
        renderedContent.find(MockedMainMenu).should.have.length.of(1);
      });

      it('should contain a single route component', () => {
        renderedContent.find(MockedRoute).should.have.length.of(1);
      });

      describe('and the route component', () => {
        let route;

        beforeEach(() => {
          route = renderedContent.find(MockedRoute);
        });

        it('should have the expected path', () => {
          const { path } = route.props();

          path.should.equal('/');
        });

        it('should have the expected component', () => {
          const { component } = route.props();

          component.should.equal(MockedHome);
        });
      });

      it('should contain a single protected route component', () => {
        renderedContent.find(MockedProtectedRoute).should.have.length.of(1);
      });

      describe('and the protected route component', () => {
        let route;

        beforeEach(() => {
          route = renderedContent.find(MockedProtectedRoute);
        });

        it('should have the expected path', () => {
          const { path } = route.props();

          path.should.equal('/game');
        });

        it('should have the expected guard', () => {
          const { guard } = route.props();

          guard.should.equal(expectedAuthGuard);
        });

        it('should have the expected component', () => {
          const { component } = route.props();

          component.should.equal(MockedGame);
        });

        it('should have the expected fallback', () => {
          const { fallback } = route.props();

          fallback.should.equal('/');
        });
      });
    });
  });
});
