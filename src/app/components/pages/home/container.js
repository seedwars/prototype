const { connect } = require('react-redux');

const template = require('./template');

const HomeComponent = () => template();

HomeComponent.displayName = 'HomeComponent';

const HomeContainer = connect()(HomeComponent);

HomeContainer.displayName = 'HomeContainer';

module.exports = HomeContainer;
