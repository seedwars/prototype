const { mockConnect } = require('./../../../../test/mocks');

const expectedContent = 'some expected content';

describe('Given the home component Container', () => {
  describe('when required', () => {
    let ContainerClass, mockedConnect, mockedRenderTemplate;

    beforeEach(() => {
      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedRenderTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedRenderTemplate);

      ContainerClass = require('./container');
    });

    describe('and when rendered', () => {
      let actualContent;

      beforeEach(() => {
        mockedRenderTemplate.returns(expectedContent);

        actualContent = ContainerClass({});
      });

      it('should be an instance of ', () => {
        actualContent.should.be.equal(expectedContent);
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
