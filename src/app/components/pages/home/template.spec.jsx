const React = require('react');
const { shallow } = require('enzyme');

const expectedText = 'Home';

describe('Given the home template', () => {
  let template;

  describe('when required as a node module', () => {
    beforeEach(() => {
      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template();

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single div element', () => {
        renderedContent.find('div').should.have.length.of(1);
      });

      describe('and the div element', () => {
        let divElement;

        beforeEach(() => {
          divElement = renderedContent.find('div');
        });

        it('should contain the expected text', () => {
          divElement.text().should.equal(expectedText);
        });
      });
    });
  });
});
