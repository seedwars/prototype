const React = require('react');
const { Button, TextField } = require('@material-ui/core');

const template = (save, updateName, name) => (
  <div>
    <TextField required label="name" onChange={ ({ target: { value } }) => updateName(value) } value={ name } />
    <Button onClick={ save }>Save</Button>
  </div>
);

module.exports = template;
