const React = require('react');
const { shallow } = require('enzyme');

const expectedName = 'some expected name';

describe('Given the character template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedTextField = () => '';
    const MockedButton = () => '';

    beforeEach(() => {
      jest.doMock('@material-ui/core', () => ({
        TextField: MockedTextField,
        Button: MockedButton,
      }));

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent, mockedSave, mockedUpdateName;

      beforeEach(() => {
        mockedSave = sandbox.spy();
        mockedUpdateName = sandbox.spy();

        HostComponent = () => template(mockedSave, mockedUpdateName, expectedName);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single TextField component', () => {
        renderedContent.find(MockedTextField).should.have.length.of(1);
      });

      describe('and the TextField component', () => {
        let textField;

        beforeEach(() => {
          textField = renderedContent.find(MockedTextField);
        });

        it('should be required', () => {
          const { required } = textField.props();

          required.should.exist();
        });

        it('should contain the expected label', () => {
          const { label } = textField.props();

          label.should.equal('name');
        });

        it('should contain the expected value', () => {
          const { value } = textField.props();

          value.should.equal(expectedName);
        });

        describe('and is changed', () => {
          beforeEach(() => {
            textField.simulate('change', { target: { value: expectedName } });
          });

          it('should update the character name', () => {
            mockedUpdateName.withArgs(expectedName).should.have.been.called();
          });
        });
      });

      it('should contain a single button component', () => {
        renderedContent.find(MockedButton).should.have.length.of(1);
      });

      describe('and the button component', () => {
        let button;

        beforeEach(() => {
          button = renderedContent.find(MockedButton);
        });

        it('should have the expected text', () => {
          button.childAt(0).text().should.equal('Save');
        });

        describe('and is clicked', () => {
          beforeEach(() => {
            button.simulate('click');
          });

          it('should save the character a single time', () => {
            mockedSave.should.have.been.calledOnce();
          });
        });
      });
    });
  });
});
