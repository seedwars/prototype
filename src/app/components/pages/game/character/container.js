const PropTypes = require('prop-types');

const { Component } = require('react');

const { connect } = require('react-redux');

const { characterService } = require('./../../../../services/game');
const { saveThunk } = require('./../../../../store/thunks/game/character');

const template = require('./template');

const mapStateToProps = ({ app: { user: { email }, character } }) => {
  const { name } = character.data;

  return { email, name };
};

const mapDispatchToProps = dispatch => ({ save: (id, name) => dispatch(saveThunk(id, name)) });

class CharacterState extends Component {
  constructor(props) {
    super(props);

    this.changeName = name => this.setState(state => ({ ...state, name }));

    this.state = { name: '' };
  }

  render() {
    const { email, save } = this.props;
    const name = this.state.name || this.props.name;

    const wrappedSave = () =>
      characterService.create(name)
        .then(character => save(email, character));

    return template(wrappedSave, this.changeName, name);
  }
}

CharacterState.displayName = 'CharacterContainer';
CharacterState.propTypes = {
  save: PropTypes.func.isRequired,
  email: PropTypes.string,
  name: PropTypes.string,
};
CharacterState.defaultProps = {
  email: '',
  name: '',
};

const CharacterContainer = connect(mapStateToProps, mapDispatchToProps)(CharacterState);

CharacterContainer.displayName = 'CharacterContainer';

module.exports = CharacterContainer;
