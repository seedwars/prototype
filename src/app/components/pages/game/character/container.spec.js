const { MockedReactComponent, mockConnect } = require('./../../../../../test/mocks');

const expectedContent = 'some expected content';
const expectedSaveThunk = 'some expected save thunk';
const expectedName = 'some expected name';
const expectedEmail = 'some expected email';

const expectedStoreUser = { email: expectedEmail };
const expectedStoreCharacter = { name: expectedName };
const expectedStoreCharacters = { data: expectedStoreCharacter };

describe('Given the character component Container', () => {
  describe('when required', () => {
    let ContainerClass, mockedConnect, mockedDispatch, mockedCharacterService,
      mockedSaveThunk, mockedRenderTemplate;

    beforeEach(() => {
      jest.doMock('react', () => ({ Component: MockedReactComponent }));

      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedConnect.setState({
        app: {
          user: expectedStoreUser,
          character: expectedStoreCharacters,
        },
      });

      mockedDispatch = sandbox.spy();
      mockedConnect.setDispatch(mockedDispatch);

      mockedCharacterService = { create: sandbox.stub() };
      jest.doMock('./../../../../services/game', () => ({ characterService: mockedCharacterService }));

      mockedSaveThunk = sandbox.stub();
      jest.doMock('./../../../../store/thunks/game/character', () => ({ saveThunk: mockedSaveThunk }));

      mockedRenderTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedRenderTemplate);

      ContainerClass = require('./container');
    });

    it('should be a function', () => {
      ContainerClass.should.be.a('function');
    });

    describe('and when the container is instantiated', () => {
      let containerInstance;

      beforeEach(() => {
        mockedRenderTemplate.returns(expectedContent);

        containerInstance = new ContainerClass();
      });

      describe('and the instance is rendered', () => {
        let actualContent;

        beforeEach(() => {
          actualContent = containerInstance.render();
        });

        it('should render the expected content', () => {
          actualContent.should.equal(expectedContent);
        });

        describe('and the template triggers the save method', () => {
          beforeEach(() => {
            mockedCharacterService.create
              .withArgs(expectedName)
              .returns(Promise.resolve(expectedStoreCharacter));
            mockedSaveThunk
              .withArgs(expectedEmail, expectedStoreCharacter)
              .returns(expectedSaveThunk);

            mockedRenderTemplate.callArg(0);
          });

          it('should dispatch the expected Save thunk', () => {
            mockedDispatch.withArgs(expectedSaveThunk).should.have.been.calledOnce();
          });
        });

        describe('and the template triggers the changeName method', () => {
          beforeEach(() => {
            mockedRenderTemplate.callArgWith(1, expectedName);
          });

          it('should dispatch the expected Save thunk', () => {
            const { name } = containerInstance.state;

            name.should.equal(expectedName);
          });
        });

        describe('and there is a valid name in the container state', () => {
          beforeEach(() => {
            containerInstance.props.name = '';
            containerInstance.state.name = expectedName;
          });

          verifyTemplateIsRenderedWithExpectedArgs(
            sinon.match.func,
            sinon.match.func,
            expectedName,
          );
        });

        describe('and there is NO valid name in the container state', () => {
          beforeEach(() => {
            containerInstance.state.name = '';
          });

          verifyTemplateIsRenderedWithExpectedArgs(
            sinon.match.func,
            sinon.match.func,
            expectedName,
          );
        });

        function verifyTemplateIsRenderedWithExpectedArgs(save, changeName, name) {
          describe('and the container is rendered', () => {
            beforeEach(() => {
              mockedRenderTemplate.resetHistory();
              containerInstance.render();
            });

            it('should have rendered the template with the expected name', () => {
              mockedRenderTemplate
                .withArgs(save, changeName, name)
                .should.have.been.calledOnce();
            });
          });
        }
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
