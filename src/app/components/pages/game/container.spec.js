const { MockedReactComponent, mockConnect } = require('./../../../../test/mocks');

const expectedContent = 'some expected content';
const expectedLoadThunk = 'some expected load thunk';
const expectedEmail = 'some expected email';

const expectedStoreUser = { email: expectedEmail };

describe('Given the game component Container', () => {
  describe('when required', () => {
    let ContainerClass, mockedRenderTemplate, mockedConnect, mockedDispatch,
      mockedCharacterLoadThunk;

    beforeEach(() => {
      jest.doMock('react', () => ({ Component: MockedReactComponent }));

      mockedCharacterLoadThunk = sandbox.stub();
      jest.doMock('./../../../store/thunks/game/character', () => ({ loadThunk: mockedCharacterLoadThunk }));

      mockedDispatch = sandbox.spy();
      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedRenderTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedRenderTemplate);

      ContainerClass = require('./container');
    });

    describe('and when the container is instantiated', () => {
      let container;

      beforeEach(() => {
        mockedConnect.setState({ app: { user: expectedStoreUser } });
        mockedConnect.setDispatch(mockedDispatch);

        container = ContainerClass({});
      });

      describe('and the component will be mounted', () => {
        beforeEach(() => {
          mockedCharacterLoadThunk.withArgs(expectedEmail).returns(expectedLoadThunk);

          container.componentWillMount();
        });

        it('should have dispatched the expected load thunk', () => {
          mockedDispatch.withArgs(expectedLoadThunk).should.have.been.calledOnce();
        });
      });

      describe('and the component is rendered', () => {
        let actualContent;

        beforeEach(() => {
          mockedRenderTemplate.returns(expectedContent);

          actualContent = container.render();
        });

        it('should render the expected content', () => {
          actualContent.should.be.equal(expectedContent);
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
