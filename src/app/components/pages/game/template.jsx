const React = require('react');

const Character = require('./character');
const Play = require('./play');

const { ProtectedRoute } = require('./../../core');
const {
  hasCharacterGuard,
  hasNoCharacterGuard,
} = require('./../../../utils/guards');


const template = match => (
  <div>
    <ProtectedRoute
      path={ `${ match.url }/character` }
      component={ Character }
      guard={ hasNoCharacterGuard }
      fallback="/game/play"
    />
    <ProtectedRoute
      path={ `${ match.url }/play` }
      component={ Play }
      guard={ hasCharacterGuard }
      fallback="/game/character"
    />
  </div>
);

module.exports = template;
