const expectedContent = 'some expected content';

const expectedPixiGlobalValue = 'some pixi global value';

describe('Given the play component Container', () => {
  describe('when required', () => {
    let ContainerClass, mockedRenderTemplate;

    beforeEach(() => {
      mockedRenderTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedRenderTemplate);

      global.PIXI = {
        settings: { SCALE_MODE: '' },
        SCALE_MODES: { NEAREST: expectedPixiGlobalValue },
      };

      ContainerClass = require('./container');
    });

    it('should be a function', () => {
      ContainerClass.should.be.a('function');
    });

    describe('and when the container is instantiated', () => {
      let actualContent;

      beforeEach(() => {
        mockedRenderTemplate.returns(expectedContent);

        actualContent = ContainerClass({});
      });

      it('should set the pixi scale mode to the expected value', () => {
        global.PIXI.settings.SCALE_MODE.should.equal(expectedPixiGlobalValue);
      });

      it('should render the expected content', () => {
        actualContent.should.equal(expectedContent);
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();

    delete global.PIXI;
  });
});
