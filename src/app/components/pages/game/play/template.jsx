const React = require('react');

const { Stage } = require('@inlet/react-pixi');

const Bunny = require('./bunny');

const playTemplate = () => (
  <Stage width={ 500 } height={ 500 } options={ { backgroundColor: 0x012b30 } }>
    <Bunny />
  </Stage>
);

module.exports = playTemplate;
