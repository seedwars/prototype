const React = require('react');
const { shallow } = require('enzyme');

const expectedRabbitUrl = 'some expected rabbit url';
const expectedRotation = 'some expected rotation';

describe('Given the bunny template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedSprite = () => '';

    beforeEach(() => {
      jest.doMock('@inlet/react-pixi', () => ({ Sprite: MockedSprite }));
      jest.doMock('./../../../../../../assets/images/rabbit.png', () => expectedRabbitUrl);

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template(expectedRotation);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single sprite component', () => {
        renderedContent.find(MockedSprite).should.have.length.of(1);
      });

      describe('and the sprite component', () => {
        let stage;

        beforeEach(() => {
          stage = renderedContent.find(MockedSprite);
        });

        it('should contain the expected image prop', () => {
          const { image } = stage.props();

          image.should.equal(expectedRabbitUrl);
        });

        it('should contain the expected x prop', () => {
          const { x } = stage.props();

          x.should.equal(100);
        });

        it('should contain the expected y prop', () => {
          const { y } = stage.props();

          y.should.equal(100);
        });

        it('should contain the expected scale prop', () => {
          const { scale } = stage.props();

          scale.should.eql([ 1, 1 ]);
        });

        it('should contain the expected anchor prop', () => {
          const { anchor } = stage.props();

          anchor.should.eql([ 0.5, 0.5 ]);
        });

        it('should contain the expected rotation prop', () => {
          const { rotation } = stage.props();

          rotation.should.equal(expectedRotation);
        });
      });
    });
  });
});
