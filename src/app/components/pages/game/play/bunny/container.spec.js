const {
  MockedReactComponent,
  mockConnect,
  mockWithPixiApp,
} = require('./../../../../../../test/mocks');

const expectedContent = 'some expected content';
const expectedDelta = 42;
const expectedRotation = 'some expected rotation';

describe('Given the bunny component Container', () => {
  describe('when required', () => {
    let ContainerClass, mockedTickerAdd, mockedTickerRemove, mockedPixiApp,
      mockedWithPixiApp, mockedConnect, mockedRenderTemplate;

    beforeEach(() => {
      mockedTickerAdd = sandbox.spy();
      mockedTickerRemove = sandbox.spy();
      mockedPixiApp = {
        ticker: {
          add: mockedTickerAdd,
          remove: mockedTickerRemove,
        },
      };
      mockedWithPixiApp = mockWithPixiApp(mockedPixiApp);
      jest.doMock('@inlet/react-pixi', () => ({ withPixiApp: mockedWithPixiApp }));

      jest.doMock('react', () => ({ Component: MockedReactComponent }));

      mockedConnect = mockConnect();
      jest.doMock('react-redux', () => ({ connect: mockedConnect }));

      mockedRenderTemplate = sandbox.stub();
      jest.doMock('./template', () => mockedRenderTemplate);

      ContainerClass = require('./container');
    });

    it('should be a function', () => {
      ContainerClass.should.be.a('function');
    });

    describe('and when the container is instantiated', () => {
      let container;

      beforeEach(() => {
        container = new ContainerClass({});
      });

      it('should be an instance of the React Component', () => {
        container.should.be.an.instanceOf(MockedReactComponent);
      });

      it('should have the initial rotation state of 0', () => {
        container.state.rotation.should.equal(0);
      });

      describe('and when the container is mounted', () => {
        beforeEach(() => {
          container.componentDidMount();
        });

        it('should have registered the tick method with the pixi app a single time', () => {
          mockedTickerAdd.withArgs(container.tick).should.have.been.calledOnce();
        });
      });

      describe('and when the container is unmounted', () => {
        beforeEach(() => {
          container.componentWillUnmount();
        });

        it('should have unregistered the tick method from the pixi app a single time', () => {
          mockedTickerRemove.withArgs(container.tick).should.have.been.calledOnce();
        });
      });

      describe('and when the updating the game', () => {
        const initialRotation = 999;

        beforeEach(() => {
          container.setState(() => ({ rotation: initialRotation }));
          container.tick(expectedDelta);
        });

        it('should update the state rotation to the expected value', () => {
          container.state.rotation.should.equal(initialRotation + (0.1 * expectedDelta));
        });
      });

      describe('and when the container is rendered', () => {
        let actualContent;

        beforeEach(() => {
          container.state.rotation = expectedRotation;

          mockedRenderTemplate
            .withArgs(container.state.rotation)
            .returns(expectedContent);

          actualContent = container.render();
        });

        it('return the expected render', () => {
          actualContent.should.equal(expectedContent);
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
