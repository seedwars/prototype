const React = require('react');

const { Sprite } = require('@inlet/react-pixi');

const rabbitUrl = require('./../../../../../../assets/images/rabbit.png');

const template = rotation => (
  <Sprite
    image={ rabbitUrl }
    x={ 100 }
    y={ 100 }
    scale={ [ 1, 1 ] }
    anchor={ [ 0.5, 0.5 ] }
    rotation={ rotation }
  />
);

module.exports = template;
