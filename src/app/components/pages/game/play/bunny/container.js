const { withPixiApp } = require('@inlet/react-pixi');
const PropTypes = require('prop-types');
const React = require('react');
const { connect } = require('react-redux');

const template = require('./template');

@withPixiApp
class BunnyComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = { rotation: 0 };
    this.tick = this.tick.bind(this);
  }

  componentDidMount() {
    this.props.app.ticker.add(this.tick);
  }

  componentWillUnmount() {
    this.props.app.ticker.remove(this.tick);
  }

  tick(delta) {
    this.setState(state => {
      const rotation = this.state.rotation + (0.1 * delta);

      return { ...state, rotation };
    });
  }

  render() {
    return template(this.state.rotation);
  }
}

BunnyComponent.displayName = 'BunnyComponent';
BunnyComponent.propTypes = { app: PropTypes.shape({ ticker: {} }) };
BunnyComponent.defaultProps = { app: null };

const BunnyContainer = connect()(BunnyComponent);

BunnyContainer.displayName = 'BunnyContainer';

module.exports = BunnyContainer;
