const React = require('react');
const { shallow } = require('enzyme');

describe('Given the play template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedStage = () => '';
    const MockedBunny = () => '';

    beforeEach(() => {
      jest.doMock('@inlet/react-pixi', () => ({ Stage: MockedStage }));
      jest.doMock('./bunny', () => MockedBunny);

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent;

      beforeEach(() => {
        HostComponent = () => template();

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single stage component', () => {
        renderedContent.find(MockedStage).should.have.length.of(1);
      });

      describe('and the stage component', () => {
        let stage;

        beforeEach(() => {
          stage = renderedContent.find(MockedStage);
        });

        it('should contain the expected width prop', () => {
          const { width } = stage.props();

          width.should.equal(500);
        });

        it('should contain the expected height prop', () => {
          const { height } = stage.props();

          height.should.equal(500);
        });

        it('should contain the expected background color prop', () => {
          const { backgroundColor } = stage.props().options;

          backgroundColor.should.equal(0x012b30);
        });

        it('should contain a single bunny component', () => {
          stage.find(MockedBunny).should.have.length.of(1);
        });
      });
    });
  });
});
