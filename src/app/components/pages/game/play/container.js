const template = require('./template');

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

const PlayContainer = () => template();

module.exports = PlayContainer;
