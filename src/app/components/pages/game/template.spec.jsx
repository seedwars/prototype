const React = require('react');
const { shallow } = require('enzyme');

const expectedUrl = 'some expected url';

describe('Given the game template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedProtectedRoute = () => '';
    const MockedCharacter = () => 'mocked character';
    const MockedPlay = () => 'mocked bunny';
    const MockedHasCharacterGuard = () => 'mocked has character guard';
    const MockedHasNoCharacterGuard = () => 'mocked has no character guard';

    beforeEach(() => {
      jest.doMock('./../../core', () => ({ ProtectedRoute: MockedProtectedRoute }));
      jest.doMock('./../../../utils/guards', () => ({
        hasCharacterGuard: MockedHasCharacterGuard,
        hasNoCharacterGuard: MockedHasNoCharacterGuard,
      }));

      jest.doMock('./character', () => MockedCharacter);
      jest.doMock('./play', () => MockedPlay);

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, renderedContent, mockedMatch;

      beforeEach(() => {
        mockedMatch = { url: expectedUrl };

        HostComponent = () => template(mockedMatch);

        renderedContent = shallow(<HostComponent />);
      });

      it('should contain a single Route component mapping the Character component to the expected path', () => {
        const path = `${ expectedUrl }/character`;
        const component = MockedCharacter;
        const guard = MockedHasNoCharacterGuard;
        const fallback = '/game/play';

        renderedContent
          .find(MockedProtectedRoute)
          .find({ component, path, guard, fallback })
          .should.have.length.of(1);
      });

      it('should contain a single Route component mapping the Play component to the expected path', () => {
        const path = `${ expectedUrl }/play`;
        const component = MockedPlay;
        const guard = MockedHasCharacterGuard;
        const fallback = '/game/character';

        renderedContent
          .find(MockedProtectedRoute)
          .find({ component, path, guard, fallback })
          .should.have.length.of(1);
      });
    });
  });
});
