const PropTypes = require('prop-types');
const { Component } = require('react');

const { connect } = require('react-redux');

const template = require('./template');

const { loadThunk: characterLoadThunk } = require('./../../../store/thunks/game/character');

const mapStateToProps = ({ app: { user: { email } } }) => ({ email });

const mapDispatchToProps = dispatch => ({ loadCharacter: id => dispatch(characterLoadThunk(id)) });

class GameState extends Component {
  componentWillMount() {
    const { email, loadCharacter } = this.props;

    loadCharacter(email);
  }

  render() {
    const { match } = this.props;

    return template(match);
  }
}

GameState.displayName = 'GameState';
GameState.propTypes = {
  email: PropTypes.string,
  loadCharacter: PropTypes.func.isRequired,
  match: PropTypes.shape({ url: PropTypes.string }).isRequired,
};
GameState.defaultProps = { email: '' };

const GameContainer = connect(mapStateToProps, mapDispatchToProps)(GameState);

GameContainer.displayName = 'GameContainer';

module.exports = GameContainer;
