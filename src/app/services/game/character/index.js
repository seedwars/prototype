const emptyCharacter = { name: '', seed: null };

class CharacterService {
  constructor() {
    this.create = name => Promise.resolve({ name });

    this.load = id => {
      const promise = new Promise(resolve => {
        const data = window.localStorage.getItem(`character.${ id }`);
        const character = data ? JSON.parse(data) : emptyCharacter;

        resolve(character);
      });

      return promise;
    };

    this.save = (id, character) => {
      const promise = new Promise(resolve => {
        const data = JSON.stringify(character);

        window.localStorage.setItem(`character.${ id }`, data);

        resolve();
      });

      return promise;
    };
  }
}

module.exports = new CharacterService();
