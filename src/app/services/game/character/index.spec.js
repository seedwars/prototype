const expectedCharacterId = 'some expected character id';
const expectedCharacterName = 'some expected character name';

describe('Given the game character service', () => {
  let service;

  describe('when required as a node module', () => {
    beforeEach(() => {
      window.localStorage.getItem = sandbox.stub();
      window.localStorage.setItem = sandbox.spy();

      service = require('./index');
    });

    it('should implement the create method', () => {
      service.should.respondTo('create');
    });

    describe('and when creating a new character', () => {
      let actualCharacter;

      beforeEach(() => service.create(expectedCharacterName)
        .then(character => { actualCharacter = character; }));

      it('should resolve with a character with the expected name', () => {
        const { name } = actualCharacter;

        name.should.equal(expectedCharacterName);
      });
    });

    it('should implement the load method', () => {
      service.should.respondTo('load');
    });

    describe('and when loading the character', () => {
      describe('and when the character does NOT exists', () => {
        const expectedEmptyCharacter = { name: '', seed: null };

        verifyCharacter(expectedCharacterId, null, expectedEmptyCharacter);
      });

      describe('and when the character does exists', () => {
        const savedCharacter = { name: 'some expected character name', seed: 'some expected character seed' };

        verifyCharacter(expectedCharacterId, JSON.stringify(savedCharacter), savedCharacter);
      });

      function verifyCharacter(characterId, savedCharacter, expectedCharacter) {
        let actualCharacter;

        beforeEach(() => {
          window.localStorage.getItem
            .withArgs(`character.${ characterId }`)
            .returns(savedCharacter);

          return service.load(characterId)
            .then(character => { actualCharacter = character; });
        });

        it('should return an empty character', () => {
          actualCharacter.should.eql(expectedCharacter);
        });
      }
    });

    it('should implement the save method', () => {
      service.should.respondTo('save');
    });

    describe('and when saving the character', () => {
      describe('and when the character does exists', () => {
        const character = { name: 'some expected character name', seed: 'some expected character seed' };

        beforeEach(() => service.save(expectedCharacterId, character));

        it('should have store the expected character ', () => {
          const id = `character.${ expectedCharacterId }`;
          const data = JSON.stringify(character);

          window.localStorage.setItem.withArgs(id, data).should.have.been.calledOnce();
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
  });
});
