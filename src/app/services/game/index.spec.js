const generateModuleConfig = (name, path, mock) => ({ name, path, mock });

const modules = [
  generateModuleConfig('characterService', './character', 'character'),
];

describe('Given the game service', () => {
  let service;

  describe('when required as a node module', () => {
    beforeEach(() => {
      modules.forEach(({ path, mock }) => jest.doMock(path, () => `some expected '${ mock }'`));

      service = require('./index');
    });

    modules.forEach(({ name, mock }) => it(`should contain the '${ name }' module`, () => {
      expect(service[name]).to.equal(`some expected '${ mock }'`);
    }));
  });

  afterEach(() => {
    jest.resetModules();
  });
});
