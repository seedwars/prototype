const google = require('./google');
const storeService = require('./store');

module.exports = { google, storeService };
