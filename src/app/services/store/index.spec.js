const expectedAction = 'some expected action';
const expectedState = 'some expected state';

describe('Given the store service Index service', () => {
  describe('when required', () => {
    let service;

    beforeEach(() => {
      service = require('./index');
    });

    it('should be an object', () => {
      service.should.be.an.instanceof(Object);
    });

    describe('and the service is NOT initialized', () => {
      describe('and an action is dispatched', () => {
        it('should throw an error', done => {
          try {
            service.dispatch(expectedAction);
            done.fail('Error not triggered');
          } catch (error) {
            done();
          }
        });
      });

      describe('and the state is requested', () => {
        it('should throw an error', done => {
          try {
            service.getState();
            done.fail('Error not triggered');
          } catch (error) {
            done();
          }
        });
      });
    });

    describe('and the service is initialized', () => {
      let mockedDispatch, mockedGetState;

      beforeEach(() => {
        mockedDispatch = sandbox.spy();
        mockedGetState = sandbox.stub().returns(expectedState);

        service.initialize(mockedDispatch, mockedGetState);
      });

      describe('and an action is dispatched', () => {
        beforeEach(() => {
          service.dispatch(expectedAction);
        });

        it('should have dispatched the action', () => {
          mockedDispatch.withArgs(expectedAction).should.have.been.called();
        });
      });

      describe('and the state is requested', () => {
        let actualState;

        beforeEach(() => {
          actualState = service.getState();
        });

        it('should return the expected state', () => {
          actualState.should.equal(expectedState);
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
