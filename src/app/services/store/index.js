class StoreService {
  constructor() {
    this.dispatch = () => { throw Error('Service not setup yet'); };
    this.getState = () => { throw Error('Service not setup yet'); };
  }

  initialize(dispatch, getState) {
    this.dispatch = action => dispatch(action);
    this.getState = () => getState();
  }
}

module.exports = new StoreService();
