const expectedClientId = 'some expected client id';
const expectedResponseType = 'some response type';
const expectedPayload = 'some expected payload';
const expectedConfig = 'some expected config';
const expectedUser = 'some expected user';
const expectedState = 'some expected state';
const expectedProfile = 'some expected profile';

describe('Given the google service Oauth2 service', () => {
  let service, mockedLoad, mockedInit, mockedCurrentUserGet, MockedCurrentUser,
    mockedSignIn, mockedSignOut, mockedGoogleAuth, mockedGetAuthInstance;

  beforeEach(() => {
    mockedLoad = sandbox.stub();
    mockedInit = sandbox.stub();
    mockedCurrentUserGet = sandbox.stub().returns(null);
    MockedCurrentUser = { get: mockedCurrentUserGet };
    mockedSignIn = sandbox.stub();
    mockedSignOut = sandbox.stub();
    mockedGoogleAuth = {
      currentUser: MockedCurrentUser,
      signIn: mockedSignIn,
      signOut: mockedSignOut,
    };
    mockedGetAuthInstance = sandbox.stub().returns(mockedGoogleAuth);

    global.gapi = {
      load: mockedLoad,
      auth2: {
        init: mockedInit,
        getAuthInstance: mockedGetAuthInstance,
      },
    };

    service = require('./oauth2');
  });

  it('should be an object', () => {
    service.should.be.an.instanceof(Object);
  });

  describe('when the service is not initialized', () => {
    describe('and when the login is executed', () => {
      it('should throw an error', done => {
        try {
          service.login();

          done.fail('Error not thrown');
        } catch (error) {
          done();
        }
      });
    });

    describe('and when the logout is executed', () => {
      it('should throw an error', done => {
        try {
          service.logout();

          done.fail('Error not thrown');
        } catch (error) {
          done();
        }
      });
    });

    describe('and when checking if the user is logged in', () => {
      it('should throw an error', done => {
        try {
          service.isLoggedIn();

          done.fail('Error not thrown');
        } catch (error) {
          done();
        }
      });
    });

    describe('and when retrieving the current profile', () => {
      it('should throw an error', done => {
        try {
          service.getProfile();

          done.fail('Error not thrown');
        } catch (error) {
          done();
        }
      });
    });

    describe('and when it is initialized', () => {
      let actualPayload;

      beforeEach(() => {
        mockedLoad.withArgs('auth2').callsArgWith(1, Promise.resolve());
        mockedInit.withArgs({
          client_id: expectedClientId,
          response_type: expectedResponseType,
        }).returns(expectedPayload);

        return service.initialize(expectedClientId, expectedResponseType)
          .then(payload => { actualPayload = payload; });
      });

      it('should set the google auth with the expected value', () => {
        service.GoogleAuth.should.equal(mockedGoogleAuth);
      });

      it('should set the google user with the expected value', () => {
        expect(service.GoogleUser).to.be.null();
      });

      it('should return the expected payload', () => {
        actualPayload.should.equal(expectedPayload);
      });

      describe('and when it is initialized a again', () => {
        it('should throw an error', done => {
          try {
            service.initialize();

            done.fail('Error not thrown');
          } catch (error) {
            done();
          }
        });
      });

      describe('and when the login is executed', () => {
        beforeEach(() => {
          mockedCurrentUserGet.returns(expectedUser);

          mockedSignIn.withArgs(expectedConfig).returns(Promise.resolve(expectedPayload));

          return service.login(expectedConfig).then(payload => { actualPayload = payload; });
        });

        it('should set the google user with the expected value', () => {
          service.GoogleUser.should.equal(expectedUser);
        });

        it('should return the expected payload', () => {
          actualPayload.should.equal(expectedPayload);
        });
      });

      describe('and when the logout is executed', () => {
        beforeEach(() => {
          mockedCurrentUserGet.returns(expectedUser);

          mockedSignOut.returns(Promise.resolve(expectedPayload));

          return service.logout(expectedConfig).then(payload => { actualPayload = payload; });
        });

        it('should set the google user with the expected value', () => {
          service.GoogleUser.should.equal(expectedUser);
        });

        it('should return the expected payload', () => {
          actualPayload.should.equal(expectedPayload);
        });
      });

      describe('and when checking if the user is logged in', () => {
        let actualState;

        beforeEach(() => {
          service.GoogleUser = { isSignedIn: () => expectedState };

          actualState = service.isLoggedIn();
        });

        it('should return the expected value ', () => {
          actualState.should.equal(expectedState);
        });
      });

      describe('and when retrieving the current profile', () => {
        let actualProfile;

        beforeEach(() => {
          service.GoogleUser = { getBasicProfile: () => expectedProfile };

          actualProfile = service.getProfile();
        });

        it('should return the expected value ', () => {
          actualProfile.should.equal(expectedProfile);
        });
      });
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();

    delete global.gapi;
  });
});
