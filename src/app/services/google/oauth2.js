class OAuth2Service {
  constructor() {
    this.initialized = false;
    this.GoogleAuth = null;
    this.GoogleUser = null;

    this.initialize = this.initialize.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  initialize(clientId, responseType) {
    if (this.initialized) throw new Error('Service is already initialized');

    this.initialized = true;

    const config = {
      client_id: clientId,
      response_type: responseType,
    };

    return new Promise(resolve => gapi.load('auth2', resolve))
      .then(() => gapi.auth2.init(config))
      .then(payload => {
        this.GoogleAuth = gapi.auth2.getAuthInstance();
        this.GoogleUser = this.GoogleAuth.currentUser.get();

        return payload;
      });
  }

  login(config) {
    if (!this.initialized) throw Error('Service not setup yet');

    return this.GoogleAuth.signIn(config).then(payload => {
      this.GoogleUser = this.GoogleAuth.currentUser.get();

      return payload;
    });
  }

  logout() {
    if (!this.initialized) throw Error('Service not setup yet');

    return this.GoogleAuth.signOut().then(payload => {
      this.GoogleUser = this.GoogleAuth.currentUser.get();

      return payload;
    });
  }

  isLoggedIn() {
    if (!this.initialized) throw Error('Service not setup yet');

    return this.GoogleUser.isSignedIn();
  }

  getProfile() {
    if (!this.initialized) throw Error('Service not setup yet');

    return this.GoogleUser.getBasicProfile();
  }
}

module.exports = new OAuth2Service();
