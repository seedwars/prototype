const React = require('react');
const { shallow } = require('enzyme');

const expectedStore = 'some expected store';
const expectedHistory = 'some expected history';

describe('Given the base template', () => {
  let template;

  describe('when required as a node module', () => {
    const MockedProvider = () => '';
    const MockedConnectedRouter = () => '';
    const MockedMain = () => '';

    beforeEach(() => {
      jest.doMock('react-redux', () => ({ Provider: MockedProvider }));
      jest.doMock('react-router-redux', () => ({ ConnectedRouter: MockedConnectedRouter }));
      jest.doMock('./components', () => ({ Main: MockedMain }));

      template = require('./template');
    });

    it('should be a function', () => {
      template.should.be.a('function');
    });

    describe('and the template is rendered', () => {
      let HostComponent, component;

      beforeEach(() => {
        HostComponent = () => template(expectedStore, expectedHistory);

        component = shallow(<HostComponent />);
      });

      it('should contain a single provider component', () => {
        component.find(MockedProvider).should.have.length.of(1);
      });

      describe('and the provider component', () => {
        let provider;

        beforeEach(() => {
          provider = component.find(MockedProvider);
        });

        it('should have the expected store', () => {
          const { store } = provider.props();

          store.should.equal(expectedStore);
        });

        it('should contain a single connected router component', () => {
          provider.find(MockedConnectedRouter).should.have.length.of(1);
        });

        describe('and the connected router component', () => {
          let connectedRouter;

          beforeEach(() => {
            connectedRouter = provider.find(MockedConnectedRouter);
          });

          it('should have the expected history', () => {
            const { history } = connectedRouter.props();

            history.should.equal(expectedHistory);
          });

          it('should contain a single main component', () => {
            connectedRouter.find(MockedMain).should.have.length.of(1);
          });
        });
      });
    });
  });
});
