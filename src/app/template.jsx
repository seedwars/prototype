const React = require('react');
const { Provider } = require('react-redux');
const { ConnectedRouter } = require('react-router-redux');
const { Main } = require('./components');

const bootstrapView = (store, history) => (
  <Provider store={ store }>
    <ConnectedRouter history={ history }>
      <div>
        <Main />
      </div>
    </ConnectedRouter>
  </Provider>
);

module.exports = bootstrapView;
