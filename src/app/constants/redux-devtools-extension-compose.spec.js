const expectedValue = 'some expected value';

describe('Given the redux-devtools-extension-compose constant', () => {
  let constant;

  beforeEach(() => {
    // eslint-disable-next-line no-underscore-dangle
    global.window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ = expectedValue;

    constant = require('./redux-devtools-extension-compose');
  });

  it('should be the expected value', () => {
    constant.should.equal(expectedValue);
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();

    // eslint-disable-next-line no-underscore-dangle
    delete global.window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  });
});
