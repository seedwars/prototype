const generateModuleConfig = (name, path, mock) => ({ name, path, mock });

const modules = [
  generateModuleConfig('REDUX_DEVTOOLS_EXTENSION_COMPOSE', './redux-devtools-extension-compose', 'redux devtools extension compose'),
];

describe('Given the Constants index module', () => {
  let module;

  beforeEach(() => {
    modules.forEach(({ path, mock }) => jest.doMock(path, () => `some expected '${ mock }'`));

    module = require('./index');
  });

  modules.forEach(({ name, mock }) => it(`should contain the '${ name }' module`, () => {
    expect(module[name]).to.equal(`some expected '${ mock }'`);
  }));

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
