const expectedGooglePublicKey = 'some expected google public key';
const expectedElement = 'some expected element';
const expectedApplication = 'some expected application';
const expectedHistory = 'some expected history';
const expectedMainReducer = 'some expected main reducer';
const expectedRouteReducer = 'some expected route reducer';
const expectedReducer = 'some expected reducer';
const expectedProfile = 'some expected profile';
const expectedAction = 'some expected action';

describe('Given the bootstrap node module', () => {
  let bootstrap, mockedUserActions, mockedUserActionUpdate, mockedCombineReducers,
    mockedCreateBrowserHistory, mockedCreateStoreWithHistory, mockedStore,
    mockedStoreDispatch, mockedStoreGetState, mockedStoreService, mockedTemplate,
    mockedGoogleOAuth2Service, mockedGoogleOAuth2ServiceInitialize, mockedRender;

  beforeEach(() => {
    global.AUTH = { google: { publicKey: expectedGooglePublicKey } };

    jest.doMock('./store', () => ({ reducers: { mainReducer: expectedMainReducer }, actions: { UserActions: mockedUserActions } }));

    mockedUserActionUpdate = sandbox.stub();
    mockedUserActionUpdate
      .withArgs(expectedProfile)
      .returns(expectedAction);
    mockedUserActions = { updateAction: mockedUserActionUpdate };
    jest.doMock('react-router-redux', () => ({ routerReducer: expectedRouteReducer }));

    const reducersConfig = { app: expectedMainReducer, router: expectedRouteReducer };
    mockedCombineReducers = sandbox.stub().returns(null);
    mockedCombineReducers
      .withArgs(reducersConfig)
      .returns(expectedReducer);
    jest.doMock('redux', () => ({ combineReducers: mockedCombineReducers }));

    mockedCreateBrowserHistory = sandbox.stub().returns(expectedHistory);
    jest.doMock('history/createBrowserHistory', () => ({ default: mockedCreateBrowserHistory }));

    mockedStoreDispatch = sandbox.spy();
    mockedStoreGetState = sandbox.spy();
    mockedStore = { dispatch: mockedStoreDispatch, getState: mockedStoreGetState };
    mockedCreateStoreWithHistory = sandbox.stub().returns(null);
    mockedCreateStoreWithHistory
      .withArgs(expectedReducer, expectedHistory)
      .returns(mockedStore);
    jest.doMock('./utils', () => ({ createStoreWithHistory: mockedCreateStoreWithHistory }));

    mockedTemplate = sandbox.stub().returns(null);
    mockedTemplate
      .withArgs(mockedStore, expectedHistory)
      .returns(expectedApplication);
    jest.doMock('./template', () => mockedTemplate);

    mockedStoreService = { initialize: sandbox.spy() };
    jest.doMock('./services', () => ({ storeService: mockedStoreService }));

    mockedGoogleOAuth2ServiceInitialize = sandbox.stub().withArgs(expectedGooglePublicKey, 'id_token permission code');
    mockedGoogleOAuth2Service = {
      initialize: mockedGoogleOAuth2ServiceInitialize,
      getProfile: () => expectedProfile,
    };
    jest.doMock('./services/google', () => ({ oauth2Service: mockedGoogleOAuth2Service }));

    mockedRender = sandbox.spy();
    jest.doMock('react-dom', () => ({ render: mockedRender }));

    bootstrap = require('./bootstrap');
  });

  describe('when executed with a successful initialization', () => {
    let promise;

    beforeEach(() => {
      mockedGoogleOAuth2ServiceInitialize.returns(Promise.resolve());

      promise = bootstrap(expectedElement);

      return promise;
    });

    it('should return a promise', () => {
      promise.should.be.an.instanceOf(Promise);
    });

    it('should initialize the store service with the expected arguments a single time', () => {
      mockedStoreService.initialize.withArgs(mockedStoreDispatch, mockedStoreGetState)
        .should.have.been.calledOnce();
    });

    it('should dispatch the expected action', () => {
      mockedStore.dispatch.withArgs(expectedAction).should.have.been.calledOnce();
    });

    it('should render the expected application', () => {
      mockedRender.withArgs(expectedApplication, expectedElement)
        .should.have.been.calledOnce();
    });
  });

  afterEach(() => {
    sandbox.restore();
    jest.resetModules();
  });
});
