const { default: createBrowserHistory } = require('history/createBrowserHistory');
const { render } = require('react-dom');

const { combineReducers } = require('redux');

const { routerReducer } = require('react-router-redux');

const { actions: { UserActions }, reducers: { mainReducer } } = require('./store');

const { storeService } = require('./services');
const { oauth2Service } = require('./services/google');

const template = require('./template');

const { createStoreWithHistory } = require('./utils');

const bootstrap = element => {
  const reducers = combineReducers({
    app: mainReducer,
    router: routerReducer,
  });

  const history = createBrowserHistory();
  const store = createStoreWithHistory(reducers, history);
  const application = template(store, history);

  storeService.initialize(store.dispatch, store.getState);

  return oauth2Service.initialize(AUTH.google.publicKey, 'id_token permission code')
    .then(() => {
      const user = oauth2Service.getProfile();
      const action = UserActions.updateAction(user);

      store.dispatch(action);

      render(application, element);
    });
};

module.exports = bootstrap;
