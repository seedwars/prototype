const MockedReactComponent = require('./react-component');
const mockConnect = require('./react-redux-connect');
const mockWithPixiApp = require('./with-pixi-app');

module.exports = { MockedReactComponent, mockConnect, mockWithPixiApp };
