const mockWithPixiApp = app =>
  Subject => {
    const injector = props => {
      const injectedProps = { ...props, app };

      let instance;

      try {
        instance = Subject(injectedProps);
      } catch (error) {
        instance = new Subject(injectedProps);
      }

      return instance;
    };

    return injector;
  };

module.exports = mockWithPixiApp;
