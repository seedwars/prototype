class MockedReactComponent {
  constructor(props) {
    this.props = props;
    this.state = {};

    this.setState = setter => { this.state = setter(this.state); };
  }
}

module.exports = MockedReactComponent;
