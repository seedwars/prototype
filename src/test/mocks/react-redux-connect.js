const mockConnect = () => {
  let storedState, storedDispatch;

  const injector = (mapStateToProps = state => state, mapDispatchToProps = state => state) =>
    Subject =>
      props => {
        const injectedProps = {
          ...props,
          ...mapStateToProps(storedState),
          ...mapDispatchToProps(storedDispatch),
        };

        let instance;

        try {
          instance = Subject(injectedProps);
        } catch (error) {
          instance = new Subject(injectedProps);
        }

        return instance;
      };

  injector.displayName = 'MockedConnect';
  injector.setState = state => { storedState = state; };
  injector.setDispatch = dispatch => { storedDispatch = dispatch; };

  return injector;
};

module.exports = mockConnect;
