const path = require('path');
const modules = require('./config/webpack/modules');
const plugins = require('./config/webpack/plugins');
const build = require('./config/webpack/build');

module.exports = build(path, __dirname, modules, plugins);
